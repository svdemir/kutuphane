﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Xml;
using System.Collections.Generic;
using System.Collections;

namespace ktProje.Kontroller.Ayarlar
{
    public partial class UCayarlar : UserControl
    {
        public UCayarlar()
        {
            InitializeComponent();
        }

        private void btnKaydet_Click(object sender, EventArgs e)
        {
            SQLiteCommand command = new SQLiteCommand("UPDATE AYARLAR SET OKULADI = @1 , TESLIMGUNU = @2 , BARKODSIRA = @3, ARKAPLANRESIM = @4 ");
            command.Parameters.AddWithValue("@1", txtOkulAdi.Text.Trim().ToUpper());
            command.Parameters.AddWithValue("@2", numericUpDown1.Value);
            command.Parameters.AddWithValue("@3", numericUpDown2.Value);
            command.Parameters.AddWithValue("@4", numericUpDown3.Value);


            try
            {
                Temel_Metodlar.SqlKomutCalistir(command);
                MessageBox.Show("Ayarları düzenleme işlemi başarılı.", "Ayar Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception)
            {
                MessageBox.Show("Ayarları düzenleme işlemi başarısız.", "Ayar Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            command.Dispose();
        }

        private void UCayarlar_Load(object sender, EventArgs e)
        {
            string sqlKomut = "SELECT * FROM AYARLAR";
            DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(sqlKomut);
            if (ds != null)
            {
                txtOkulAdi.Text = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                numericUpDown1.Value = Convert.ToInt32(ds.Tables[0].Rows[0].ItemArray[1]);
                numericUpDown2.Value = Convert.ToInt32(ds.Tables[0].Rows[0].ItemArray[2]);       //enSonBarkodNo 
                numericUpDown3.Value = Convert.ToInt32(ds.Tables[0].Rows[0].ItemArray[3]);       //ödünç sayısı 

                ds.Dispose();
            }
            else
            {
                MessageBox.Show("İşlem gerçekleştirilemedi. Veritabanı hatası.", "Ayarlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                btnKaydet.Enabled = false;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (var DBbaglanti = new SQLiteConnection("Data Source=db.sqlite;Version=3;"))
            {

                //SQLiteCommand command = new SQLiteCommand();
                //command.Connection = DBbaglanti;
                //DBbaglanti.Open();
                //SQLiteDataAdapter adapter = new SQLiteDataAdapter(command);
                //DataSet ds = new DataSet();
                //ds.DataSetName = "yedekler";

                //command.CommandText = "select* from sqlite_master";             //tablo isimleri alınıyor.
                //adapter.Fill(ds, "TABLOLAR");

                //ArrayList tabloListesi = new ArrayList();
                //for (int i = 0; i < ds.Tables["TABLOLAR"].Rows.Count; i++)
                //{
                //    if (ds.Tables["TABLOLAR"].Rows[i].ItemArray[0].ToString() == "table")
                //    {
                //        tabloListesi.Add(ds.Tables["TABLOLAR"].Rows[i].ItemArray[1].ToString());
                //    }
                //}

                //for (int i = 0; i < tabloListesi.Count; i++)                    //tablo verileri alınıyor.
                //{
                //    command.CommandText = "SELECT * FROM " + tabloListesi[i].ToString();
                //    try
                //    {
                //        adapter.Fill(ds, tabloListesi[i].ToString());
                //    }
                //    catch (Exception ex)
                //    {
                //    }
                //}

                //ds.WriteXml("yedek.xml");                                       //xml yedek oluşturuluyor.


                //SQLiteCommand command2 = new SQLiteCommand();
                //command2.Connection = DBbaglanti;
                //SQLiteDataAdapter adapter2 = new SQLiteDataAdapter(command2);



                //DataSet yeni = new DataSet();
                //yeni.ReadXml("yedek.xml");

                //try
                //{
                //    //adapter2.Fill(yeni);

                //}
                //catch (Exception ex)
                //{
                //}


                //XmlDocument doc = new XmlDocument();
                //doc.Load("yedek.xml");
                //var altNodlar = doc.ChildNodes[1];
                //for (int i = 0; i < altNodlar.ChildNodes.Count; i++)
                //{
                //    if (altNodlar.ChildNodes[i].Name == "UYELER")
                //    {
                //        var uye = altNodlar.ChildNodes[i];
                //        string No = uye.ChildNodes[0].InnerText;
                //        string Ad = uye.ChildNodes[1].InnerText;
                //        string Soyad = uye.ChildNodes[2].InnerText;
                //    }
                //}


            }
        }           //yedekleme işlemleri yapılacak.

        private void btnTumUyeSil_Click(object sender, EventArgs e)
        {
            DialogResult cevap = MessageBox.Show("Dikkat Tüm Üyeleri Silme işlemini onaylıyor musunuz?", "Silme İşlemi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (cevap == DialogResult.Yes)
            {
                SQLiteCommand command = new SQLiteCommand("DELETE FROM UYELER");
                bool silmeDurumu = Temel_Metodlar.SqlKomutCalistir(command);
                if (silmeDurumu)
                {
                    MessageBox.Show("Tüm üyeleri silme işlemi başarılı.", "Üye Silme", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Tüm üyeleri silme işlemi başarısız.", "Üye Silme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                command.Dispose();
            }
        }

        private void btnTumKitapSil_Click(object sender, EventArgs e)
        {
            DialogResult cevap = MessageBox.Show("Dikkat Tüm Kitapları Silme işlemini onaylıyor musunuz?", "Silme İşlemi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (cevap == DialogResult.Yes)
            {
                SQLiteCommand command = new SQLiteCommand("DELETE FROM KITAPLAR");
                bool silmeDurumu = Temel_Metodlar.SqlKomutCalistir(command);
                if (silmeDurumu)
                {
                    MessageBox.Show("Tüm kitapları silme işlemi başarılı.", "Kitap Silme", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Tüm kitapları silme işlemi başarısız.", "Kitap Silme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                command.Dispose();
            }
        }

        private void btnTumIstSil_Click(object sender, EventArgs e)
        {
            DialogResult cevap = MessageBox.Show("Dikkat Tüm İstatistikleri Silme işlemini onaylıyor musunuz?", "Silme İşlemi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (cevap == DialogResult.Yes)
            {
                SQLiteCommand command = new SQLiteCommand("DELETE FROM ODUNC WHERE TESLIMDURUMU='GETİRDİ'");
                bool silmeDurumu = Temel_Metodlar.SqlKomutCalistir(command);
                if (silmeDurumu)
                {
                    MessageBox.Show("Tüm istatistikleri silme işlemi başarılı.", "İstatistikler Silme", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Tüm istatistikleri  silme işlemi başarısız.", "İstatistikler Silme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                command.Dispose();
            }
        }
    }
}
