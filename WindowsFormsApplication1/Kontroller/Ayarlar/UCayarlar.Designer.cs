﻿namespace ktProje.Kontroller.Ayarlar
{
    partial class UCayarlar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnTumKitapSil = new System.Windows.Forms.Button();
            this.btnTumUyeSil = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.txtOkulAdi = new System.Windows.Forms.TextBox();
            this.btnKaydet = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnTumIstSil = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.numericUpDown2);
            this.groupBox1.Controls.Add(this.numericUpDown3);
            this.groupBox1.Controls.Add(this.numericUpDown1);
            this.groupBox1.Controls.Add(this.txtOkulAdi);
            this.groupBox1.Controls.Add(this.btnKaydet);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(485, 364);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Genel Ayarlar";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnTumKitapSil);
            this.groupBox2.Controls.Add(this.btnTumIstSil);
            this.groupBox2.Controls.Add(this.btnTumUyeSil);
            this.groupBox2.Location = new System.Drawing.Point(26, 195);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(431, 156);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Silme İşlemleri";
            // 
            // btnTumKitapSil
            // 
            this.btnTumKitapSil.Image = global::ktProje.Properties.Resources.error;
            this.btnTumKitapSil.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTumKitapSil.Location = new System.Drawing.Point(187, 28);
            this.btnTumKitapSil.Name = "btnTumKitapSil";
            this.btnTumKitapSil.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnTumKitapSil.Size = new System.Drawing.Size(146, 54);
            this.btnTumKitapSil.TabIndex = 2;
            this.btnTumKitapSil.Text = "       Tüm Kitapları Sil";
            this.btnTumKitapSil.UseVisualStyleBackColor = true;
            this.btnTumKitapSil.Click += new System.EventHandler(this.btnTumKitapSil_Click);
            // 
            // btnTumUyeSil
            // 
            this.btnTumUyeSil.Image = global::ktProje.Properties.Resources.error;
            this.btnTumUyeSil.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTumUyeSil.Location = new System.Drawing.Point(26, 28);
            this.btnTumUyeSil.Name = "btnTumUyeSil";
            this.btnTumUyeSil.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnTumUyeSil.Size = new System.Drawing.Size(146, 54);
            this.btnTumUyeSil.TabIndex = 2;
            this.btnTumUyeSil.Text = "      Tüm Üyeleri Sil";
            this.btnTumUyeSil.UseVisualStyleBackColor = true;
            this.btnTumUyeSil.Click += new System.EventHandler(this.btnTumUyeSil_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(382, 148);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(213, 102);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(92, 29);
            this.numericUpDown2.TabIndex = 4;
            this.numericUpDown2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numericUpDown3
            // 
            this.numericUpDown3.Location = new System.Drawing.Point(242, 143);
            this.numericUpDown3.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDown3.Name = "numericUpDown3";
            this.numericUpDown3.Size = new System.Drawing.Size(63, 29);
            this.numericUpDown3.TabIndex = 4;
            this.numericUpDown3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(187, 65);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(118, 29);
            this.numericUpDown1.TabIndex = 4;
            this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtOkulAdi
            // 
            this.txtOkulAdi.Location = new System.Drawing.Point(108, 27);
            this.txtOkulAdi.Name = "txtOkulAdi";
            this.txtOkulAdi.Size = new System.Drawing.Size(197, 29);
            this.txtOkulAdi.TabIndex = 3;
            // 
            // btnKaydet
            // 
            this.btnKaydet.Image = global::ktProje.Properties.Resources.computing;
            this.btnKaydet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnKaydet.Location = new System.Drawing.Point(347, 56);
            this.btnKaydet.Name = "btnKaydet";
            this.btnKaydet.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnKaydet.Size = new System.Drawing.Size(126, 60);
            this.btnKaydet.TabIndex = 2;
            this.btnKaydet.Text = "        Güncelle";
            this.btnKaydet.UseVisualStyleBackColor = true;
            this.btnKaydet.Click += new System.EventHandler(this.btnKaydet_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 145);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(214, 21);
            this.label4.TabIndex = 1;
            this.label4.Text = "Ödünç Verilecek Kitap Sayısı";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(132, 21);
            this.label3.TabIndex = 1;
            this.label3.Text = "Kitap İade Süresi";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 104);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(188, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "En Son Kullanılan Barkod";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "Okul İsmi";
            // 
            // btnTumIstSil
            // 
            this.btnTumIstSil.Image = global::ktProje.Properties.Resources.error;
            this.btnTumIstSil.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTumIstSil.Location = new System.Drawing.Point(94, 88);
            this.btnTumIstSil.Name = "btnTumIstSil";
            this.btnTumIstSil.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnTumIstSil.Size = new System.Drawing.Size(185, 54);
            this.btnTumIstSil.TabIndex = 2;
            this.btnTumIstSil.Text = "      Tüm İstatistikleri Sil";
            this.btnTumIstSil.UseVisualStyleBackColor = true;
            this.btnTumIstSil.Click += new System.EventHandler(this.btnTumIstSil_Click);
            // 
            // UCayarlar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "UCayarlar";
            this.Size = new System.Drawing.Size(502, 381);
            this.Load += new System.EventHandler(this.UCayarlar_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnKaydet;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.TextBox txtOkulAdi;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDown3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnTumKitapSil;
        private System.Windows.Forms.Button btnTumUyeSil;
        private System.Windows.Forms.Button btnTumIstSil;
    }
}
