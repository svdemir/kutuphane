﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace ktProje.Kontroller.Ayarlar
{
    public partial class UCguncelle : UserControl
    {
        public UCguncelle()
        {
            InitializeComponent();
        }

        private void btnGuncellemeKontrol_Click(object sender, EventArgs e)
        {
            string mevcutVersiyonNo = Temel_Metodlar.VersiyonNumarasıGetir();
            string gncVersiyon = GuncellemeKontrol();
            if (mevcutVersiyonNo != gncVersiyon && !String.IsNullOrEmpty(gncVersiyon) && gncVersiyon != "")
            {
                btnGuncelle.Enabled = true;
                lblGuncelVersiyon.Text = guncelVersiyonDegeri;
            }
            else
            {
                MessageBox.Show("Yazılımınız güncel.", "Güncelleme İşlemi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblGuncelVersiyon.Text = guncelVersiyonDegeri;
            }
        }
        private void UCguncelle_Load(object sender, EventArgs e)
        {
            lblMevcutVersiyon.Text = Temel_Metodlar.VersiyonNumarasıGetir();
        }

        string guncelVersiyonDegeri;
        string indirmeLink;

        private string GuncellemeKontrol()
        {
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            string URL = "https://gist.github.com/svdemir/9cd903d41cbc5b9223715f0722361bcd";
            Uri url = new Uri(URL);
            WebClient client = new WebClient();
            string input = client.DownloadString(url);
            string patternVersiyon = "<span class=\"pl-ent\">version</span>";
            string patternLink = "<span class=\"pl-ent\">link</span>";
            string guncelVersiyonDegeriGecici = getBetween(input, patternVersiyon, patternVersiyon);
            guncelVersiyonDegeri = getBetween(guncelVersiyonDegeriGecici, ";", "&");
            string indirmeLinkGecici = getBetween(input, patternLink, patternLink);
            indirmeLink = getBetween(indirmeLinkGecici, ";", "&");
            return guncelVersiyonDegeri;
        }

        private string getBetween(string strSource, string strStart, string strEnd)
        {
            int Start, End;
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                End = strSource.IndexOf(strEnd, Start);
                return strSource.Substring(Start, End - Start);
            }
            else
            {
                return "";
            }
        }

        string tempFilePaths;
        private void GuncelVersiyonIndirme()
        {
            var pathWithEnv = @"%USERPROFILE%\";
            tempFilePaths = Environment.ExpandEnvironmentVariables(pathWithEnv);
            Directory.CreateDirectory(tempFilePaths);
            string driveLink = "https://drive.google.com/uc?id=" + indirmeLink + "&export=download";

            Uri url = new Uri(driveLink);
            WebClient client = new WebClient();

            client.DownloadFile(url, tempFilePaths + "\\ktProje.exe");
            YeniVersiyonTasimaDuzenleme();
        }

        private void YeniVersiyonTasimaDuzenleme()
        {
            string argument_update = "/C choice /C Y /N /D Y /T 4 & Del /F /Q \"{0}\" & choice /C Y /N /D Y /T 2 & Move /Y \"{1}\" \"{2}\"";
            string argument_update_start = argument_update + " & Start \"\" /D \"{3}\" \"{4}\" {5}";
            string argument_complete = "";

            string currentPaths = Application.StartupPath;
            string newPaths = Application.StartupPath;
            argument_complete = string.Format(argument_update_start, currentPaths + "\\ktProje.exe", tempFilePaths + "\\ktProje.exe", newPaths, Path.GetDirectoryName(newPaths + "ktProje.exe"), Path.GetFileName("ktProje.exe"), " ");

            ProcessStartInfo cmd = new ProcessStartInfo
            {
                Arguments = argument_complete,
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true,
                FileName = "cmd.exe"
            };

            cmd.UseShellExecute = false;
            Process.Start(cmd);
            Environment.Exit(0);
        }

        private void btnGuncelle_Click(object sender, EventArgs e)
        {
            GuncelVersiyonIndirme();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://drive.google.com/drive/folders/0B9JbYOAAnwGwV0FuRGtCTW9WNWs");
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://bitbucket.org/svdemir/kutuphane/src/");
        }
    }
}
