﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SQLite;

namespace ktProje.Kontroller.Ayarlar
{
    public partial class UCyayineviEkle : UserControl
    {
        public UCyayineviEkle()
        {
            InitializeComponent();
        }

        SQLiteDataAdapter adapter;
        DataSet ds;

        private void UCyayineviEkle_Load(object sender, EventArgs e)
        {
            string komut = "SELECT * FROM YAYINEVLERI";
            var DBbaglanti = new SQLiteConnection("Data Source=db.sqlite;Version=3;");
            SQLiteCommand command = new SQLiteCommand(komut, DBbaglanti);
            DBbaglanti.Open();
            adapter = new SQLiteDataAdapter(command);
            ds = new DataSet();
            try
            {
                adapter.Fill(ds);
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi. Veritabanı hatası.", "Yayınevi Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DBbaglanti.Close();
            SQLiteCommandBuilder cb = new SQLiteCommandBuilder(adapter);
            cb.ConflictOption = ConflictOption.OverwriteChanges;

            BindingSource sc = new BindingSource();
            sc.DataSource = ds.Tables[0];

            bindingNavigator1.BindingSource = sc;
            dataGridView1.DataSource = sc;
            dataGridView1.Columns[0].Visible = false;

            dataGridView1.Columns[1].HeaderText = "Yayınevi Adı";
            dataGridView1.Columns[1].Width = 180;
        }

        private void saveToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.dataGridView1.EndEdit();
                adapter.Update(ds);
                MessageBox.Show("İşlem başarılı.", "Yayınevi Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception )
            {
                MessageBox.Show("Hata. Alanları kontrol ediniz. Yayınevi adının aynı olmaması gerekmektedir.", "Yayınevi Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView1.BeginEdit(true);
        }
    }
}
