﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Drawing.Printing;
using Ean13Barcode2005;
using System.Data.SQLite;
using System.Linq;

namespace ktProje.Kontroller
{
    public partial class UCbarkod : UserControl
    {
        public UCbarkod()
        {
            InitializeComponent();
        }

        List<Ean13> barkodImajDizisi = new List<Ean13>();
        List<Ean13> barkodImajDizisi2 = new List<Ean13>();

        string okuladi;

        private void btnYazdir_Click(object sender, EventArgs e)                                // son barkod veritabanından çekilip yazdırılıyor, daha sonra son barkod güncelleniyor.
        {
            PrintDocument doc = new PrintDocument();
            string sqlKomut = "SELECT * FROM AYARLAR";
            DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(sqlKomut);
            if (ds != null)
            {
                int enSonBarkodNo = Convert.ToInt32(ds.Tables[0].Rows[0].ItemArray[2]);
                okuladi = ds.Tables[0].Rows[0].ItemArray[0].ToString();

                for (int i = 0; i < numericUpDown1.Value; i++)
                {
                    Ean13 barcode = new Ean13();
                    barcode.ManufacturerCode = "00000";
                    barcode.ProductCode = enSonBarkodNo.ToString();
                    barkodImajDizisi.Add(barcode);
                    enSonBarkodNo++;
                }

                SQLiteCommand sqlGuncelleKomut = new SQLiteCommand("UPDATE AYARLAR SET BARKODSIRA = @1");
                sqlGuncelleKomut.Parameters.AddWithValue("@1", enSonBarkodNo);
                Temel_Metodlar.SqlKomutCalistir(sqlGuncelleKomut);

                doc.PrintPage += new PrintPageEventHandler(doc_PrintPage);
                doc.Print();
                this.Dispose();
                ds.Dispose();
                MessageBox.Show("Barkod yazdırma işlemi başarılı.", "Barkad", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("İşlem gerçekleştirilemedi. Veritabanı hatası.", "Barkod", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void doc_PrintPage(object sender, PrintPageEventArgs e)                         // toplu barkod yazdırılıyor.
        {
            List<int> barkodDizisi = new List<int>(new int[] { 25, 185, 65, 345, 105, 505, 145, 665, 185, 825, 225, 985 });
            for (int i = 0; i < (numericUpDown1.Value / 4); i++)
            {
                barkodImajDizisi[0].DrawEan13Barcode(e.Graphics, (new PointF(Convert.ToInt32(15), Convert.ToInt32(barkodDizisi[0]))));
                e.Graphics.DrawString(okuladi, new System.Drawing.Font(new FontFamily("Arial"), 8, FontStyle.Bold), Brushes.Black, new PointF(65, barkodDizisi[1]));

                barkodImajDizisi[1].DrawEan13Barcode(e.Graphics, (new PointF(Convert.ToInt32(60), Convert.ToInt32(barkodDizisi[0]))));
                e.Graphics.DrawString(okuladi, new System.Drawing.Font(new FontFamily("Arial"), 8, FontStyle.Bold), Brushes.Black, new PointF(245, barkodDizisi[1]));

                barkodImajDizisi[2].DrawEan13Barcode(e.Graphics, (new PointF(Convert.ToInt32(105), Convert.ToInt32(barkodDizisi[0]))));
                e.Graphics.DrawString(okuladi, new System.Drawing.Font(new FontFamily("Arial"), 8, FontStyle.Bold), Brushes.Black, new PointF(425, barkodDizisi[1]));

                barkodImajDizisi[3].DrawEan13Barcode(e.Graphics, (new PointF(Convert.ToInt32(150), Convert.ToInt32(barkodDizisi[0]))));
                e.Graphics.DrawString(okuladi, new System.Drawing.Font(new FontFamily("Arial"), 8, FontStyle.Bold), Brushes.Black, new PointF(605, barkodDizisi[1]));

                barkodDizisi.RemoveRange(0, 2);
                barkodImajDizisi.RemoveRange(0, 4);
            }
        }

        private void btnYazdir2_Click(object sender, EventArgs e)                               // manuel barkod yazdırma.
        {
            string sqlKomut = "SELECT * FROM AYARLAR";
            DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(sqlKomut);
            if (ds != null)
            {
                okuladi = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                ds.Dispose();

                foreach (var numeric in groupBox2.Controls.OfType<NumericUpDown>())
                {
                    if (!string.IsNullOrEmpty(numeric.Text))
                    {
                        Ean13 barcode = new Ean13();
                        barcode.ManufacturerCode = "000000";
                        barcode.ChecksumDigit = numeric.Text.Substring(numeric.Text.Length - 1, 1);
                        barcode.ProductCode = numeric.Text.Substring(0, numeric.Text.Length - 1);
                        barkodImajDizisi2.Add(barcode);
                    }
                    else
                    {
                        barkodImajDizisi2.Add(null);
                    }
                }

                PrintDocument doc = new PrintDocument();
                doc.PrintPage += new PrintPageEventHandler(doc_PrintPage2);

                doc.Print();
                this.Dispose();
                ds.Dispose();
                MessageBox.Show("Barkod yazdırma işlemi başarılı.", "Barkad", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("İşlem gerçekleştirilemedi. Veritabanı hatası.", "Barkod", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void doc_PrintPage2(object sender, PrintPageEventArgs e)
        {
            if (barkodImajDizisi2[0] != null)
            {
                barkodImajDizisi2[0].DrawEan13Barcode(e.Graphics, (new PointF(Convert.ToInt32(15), 25)));
                e.Graphics.DrawString(okuladi, new System.Drawing.Font(new FontFamily("Arial"), 8, FontStyle.Bold), Brushes.Black, new PointF(65, 185));
            }

            if (barkodImajDizisi2[1] != null)
            {
                barkodImajDizisi2[1].DrawEan13Barcode(e.Graphics, (new PointF(Convert.ToInt32(60), 25)));
                e.Graphics.DrawString(okuladi, new System.Drawing.Font(new FontFamily("Arial"), 8, FontStyle.Bold), Brushes.Black, new PointF(245, 185));
            }

            if (barkodImajDizisi2[2] != null)
            {
                barkodImajDizisi2[2].DrawEan13Barcode(e.Graphics, (new PointF(Convert.ToInt32(105), 25)));
                e.Graphics.DrawString(okuladi, new System.Drawing.Font(new FontFamily("Arial"), 8, FontStyle.Bold), Brushes.Black, new PointF(425, 185));
            }

            if (barkodImajDizisi2[3] != null)
            {
                barkodImajDizisi2[3].DrawEan13Barcode(e.Graphics, (new PointF(Convert.ToInt32(150), 25)));
                e.Graphics.DrawString(okuladi, new System.Drawing.Font(new FontFamily("Arial"), 8, FontStyle.Bold), Brushes.Black, new PointF(605, 185));
            }
        }

        private void UCbarkod_Load(object sender, EventArgs e)
        {
            foreach (var numeric in groupBox2.Controls.OfType<NumericUpDown>())
            {
                numeric.Text = "";
            }
        }
    }
}
