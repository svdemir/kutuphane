﻿namespace ktProje.Kontroller.Ayarlar
{
    partial class UCguncelle
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.btnGuncellemeKontrol = new System.Windows.Forms.Button();
            this.btnGuncelle = new System.Windows.Forms.Button();
            this.lblGuncelVersiyon = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblMevcutVersiyon = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.linkLabel2);
            this.groupBox1.Controls.Add(this.linkLabel1);
            this.groupBox1.Controls.Add(this.btnGuncellemeKontrol);
            this.groupBox1.Controls.Add(this.btnGuncelle);
            this.groupBox1.Controls.Add(this.lblGuncelVersiyon);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.lblMevcutVersiyon);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(493, 317);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Güncelleme İşlemleri";
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.LinkColor = System.Drawing.Color.Blue;
            this.linkLabel2.Location = new System.Drawing.Point(396, 281);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(58, 21);
            this.linkLabel2.TabIndex = 3;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "Kodlar";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.LinkColor = System.Drawing.Color.Blue;
            this.linkLabel1.Location = new System.Drawing.Point(341, 249);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(125, 21);
            this.linkLabel1.TabIndex = 3;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Tüm Versiyonlar";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // btnGuncellemeKontrol
            // 
            this.btnGuncellemeKontrol.Image = global::ktProje.Properties.Resources.uye1;
            this.btnGuncellemeKontrol.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuncellemeKontrol.Location = new System.Drawing.Point(283, 32);
            this.btnGuncellemeKontrol.Name = "btnGuncellemeKontrol";
            this.btnGuncellemeKontrol.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnGuncellemeKontrol.Size = new System.Drawing.Size(135, 60);
            this.btnGuncellemeKontrol.TabIndex = 2;
            this.btnGuncellemeKontrol.Text = "       Kontrol Et";
            this.btnGuncellemeKontrol.UseVisualStyleBackColor = true;
            this.btnGuncellemeKontrol.Click += new System.EventHandler(this.btnGuncellemeKontrol_Click);
            // 
            // btnGuncelle
            // 
            this.btnGuncelle.Enabled = false;
            this.btnGuncelle.Image = global::ktProje.Properties.Resources.computing;
            this.btnGuncelle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuncelle.Location = new System.Drawing.Point(283, 110);
            this.btnGuncelle.Name = "btnGuncelle";
            this.btnGuncelle.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnGuncelle.Size = new System.Drawing.Size(135, 60);
            this.btnGuncelle.TabIndex = 2;
            this.btnGuncelle.Text = "        Güncelle";
            this.btnGuncelle.UseVisualStyleBackColor = true;
            this.btnGuncelle.Click += new System.EventHandler(this.btnGuncelle_Click);
            // 
            // lblGuncelVersiyon
            // 
            this.lblGuncelVersiyon.AutoSize = true;
            this.lblGuncelVersiyon.Location = new System.Drawing.Point(159, 130);
            this.lblGuncelVersiyon.Name = "lblGuncelVersiyon";
            this.lblGuncelVersiyon.Size = new System.Drawing.Size(14, 21);
            this.lblGuncelVersiyon.TabIndex = 1;
            this.lblGuncelVersiyon.Text = ".";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 281);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(376, 21);
            this.label5.TabIndex = 1;
            this.label5.Text = "*Projenin kaynak kodlarına adresten ulaşabilirsiniz. ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 249);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(322, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "*Güncel versiyonu adresten indirebilirsiniz. ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 217);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(438, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "*Güncelleme bittikten sonra lütfen programı kapatıp açınız.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(130, 21);
            this.label4.TabIndex = 1;
            this.label4.Text = "Güncel Versiyon:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 21);
            this.label3.TabIndex = 1;
            this.label3.Text = "Mevcut Versiyon:";
            // 
            // lblMevcutVersiyon
            // 
            this.lblMevcutVersiyon.AutoSize = true;
            this.lblMevcutVersiyon.Location = new System.Drawing.Point(164, 52);
            this.lblMevcutVersiyon.Name = "lblMevcutVersiyon";
            this.lblMevcutVersiyon.Size = new System.Drawing.Size(14, 21);
            this.lblMevcutVersiyon.TabIndex = 1;
            this.lblMevcutVersiyon.Text = ".";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 185);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(419, 21);
            this.label6.TabIndex = 1;
            this.label6.Text = "*Güncellemeye yapılırken lütfen birkaç dakika bekleyiniz.";
            // 
            // UCguncelle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "UCguncelle";
            this.Size = new System.Drawing.Size(507, 344);
            this.Load += new System.EventHandler(this.UCguncelle_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnGuncellemeKontrol;
        private System.Windows.Forms.Button btnGuncelle;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblMevcutVersiyon;
        private System.Windows.Forms.Label lblGuncelVersiyon;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}
