﻿namespace ktProje.Kontroller.Istatistik
{
    partial class UCstatistikDuzenle
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnStatistikSil = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnOgrNoGetir = new System.Windows.Forms.Button();
            this.numOduncOgrNo = new System.Windows.Forms.NumericUpDown();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOduncOgrNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnStatistikSil);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnOgrNoGetir);
            this.groupBox1.Controls.Add(this.numOduncOgrNo);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox1.Location = new System.Drawing.Point(15, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(767, 102);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "İstatistik  Silme";
            // 
            // btnStatistikSil
            // 
            this.btnStatistikSil.Enabled = false;
            this.btnStatistikSil.Image = global::ktProje.Properties.Resources.error;
            this.btnStatistikSil.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStatistikSil.Location = new System.Drawing.Point(507, 34);
            this.btnStatistikSil.Name = "btnStatistikSil";
            this.btnStatistikSil.Padding = new System.Windows.Forms.Padding(6, 0, 0, 0);
            this.btnStatistikSil.Size = new System.Drawing.Size(118, 49);
            this.btnStatistikSil.TabIndex = 12;
            this.btnStatistikSil.Text = "     Sil";
            this.btnStatistikSil.UseVisualStyleBackColor = true;
            this.btnStatistikSil.Click += new System.EventHandler(this.btnStatistikSil_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(92, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 21);
            this.label2.TabIndex = 11;
            this.label2.Text = "Öğrenci No";
            // 
            // btnOgrNoGetir
            // 
            this.btnOgrNoGetir.Location = new System.Drawing.Point(282, 47);
            this.btnOgrNoGetir.Name = "btnOgrNoGetir";
            this.btnOgrNoGetir.Size = new System.Drawing.Size(75, 28);
            this.btnOgrNoGetir.TabIndex = 10;
            this.btnOgrNoGetir.Text = "Getir";
            this.btnOgrNoGetir.UseVisualStyleBackColor = true;
            this.btnOgrNoGetir.Click += new System.EventHandler(this.btnOgrNoGetir_Click);
            // 
            // numOduncOgrNo
            // 
            this.numOduncOgrNo.Location = new System.Drawing.Point(195, 46);
            this.numOduncOgrNo.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numOduncOgrNo.Name = "numOduncOgrNo";
            this.numOduncOgrNo.Size = new System.Drawing.Size(72, 29);
            this.numOduncOgrNo.TabIndex = 9;
            this.numOduncOgrNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(16, 120);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(766, 291);
            this.dataGridView1.TabIndex = 2;
            // 
            // UCstatistikDuzenle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "UCstatistikDuzenle";
            this.Size = new System.Drawing.Size(789, 423);
            this.Load += new System.EventHandler(this.UCstatistikDuzenle_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOduncOgrNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnOgrNoGetir;
        private System.Windows.Forms.NumericUpDown numOduncOgrNo;
        private System.Windows.Forms.Button btnStatistikSil;
    }
}
