﻿using System;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using DGVPrinterHelper;
using System.Data.SQLite;

namespace ktProje.Kontroller.Istatistik
{
    public partial class UCogrenciOkunanlar : UserControl
    {
        public UCogrenciOkunanlar()
        {
            InitializeComponent();
        }

        string aralikBaslangic;
        string aralikBitis;


        private void btnOkunanKitaplariGetir_Click(object sender, EventArgs e)
        {
            if (radioButton3.Checked)                   // tüm zamanlar
            {
                aralikBaslangic = "00000000";
                aralikBitis = "99991230";
            }
            if (radioButton1.Checked)                   // son bir ay
            {
                aralikBaslangic = DateTime.Today.AddMonths(-1).ToString("yyyyMMdd");
                aralikBitis = DateTime.Today.ToString("yyyyMMdd");
            }
            if (radioButton2.Checked)
            {
                aralikBaslangic = (DateTime.Today.Year - 1).ToString() + "0915";
                aralikBitis = DateTime.Today.ToString("yyyyMMdd");
            }
            if (radioButton4.Checked)
            {
                aralikBaslangic = dateTimePicker1.Value.ToString("yyyyMMdd");
                aralikBitis = dateTimePicker2.Value.ToString("yyyyMMdd");
            }

            OgrenciOkuduguKitaplarGetir(numericOgrenciNo.Value.ToString());



            Temel_Metodlar.Numaralandir(dataGridView1);
            dataGridView1.AllowUserToAddRows = false;
            btnKitapListesiYazdır.Enabled = true;
        }

        private void OgrenciOkuduguKitaplarGetir(string ogrenciNo)
        {
            SQLiteCommand komut2 = new SQLiteCommand(@"SELECT k.ADI,o.OGRALISTARIHI,o.OGRTESLIMTARIHI,o.GECIKMEGUNU FROM ODUNC o INNER JOIN KITAPLAR k on o.BARKOD  = k.BARKOD WHERE OGRNO=@1 AND TESLIMDURUMU=@2 AND substr(OGRTESLIMTARIHI, -4)   ||  substr(OGRTESLIMTARIHI, instr('OGRTESLIMTARIHI', '')-6  ,-2)     ||  substr(OGRTESLIMTARIHI, instr('OGRTESLIMTARIHI', '')-9  ,-2) between @3 and @4 ");
            komut2.Parameters.AddWithValue("@1", ogrenciNo);
            komut2.Parameters.AddWithValue("@2", "GETİRDİ");

            komut2.Parameters.AddWithValue("@3", aralikBaslangic);          //  başlangıç tarihi
            komut2.Parameters.AddWithValue("@4", aralikBitis);          //  bitiş  tarihi

            try
            {
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut2);
                dataGridView1.DataSource = ds.Tables[0];
                komut2.Dispose();
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi.", "Kitap Okuyanlar Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            dataGridView1.Columns[0].HeaderText = "Kitap Adı";
            dataGridView1.Columns[1].HeaderText = "Alış Tarihi";
            dataGridView1.Columns[2].HeaderText = "Teslim Tarihi";
            dataGridView1.Columns[3].HeaderText = "Gecikme Günü";

            dataGridView1.Columns[0].Width = 175;
            dataGridView1.Columns[1].Width = 90;
            dataGridView1.Columns[2].Width = 90;
            dataGridView1.Columns[3].Width = 70;
            dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;


            Temel_Metodlar.Numaralandir(dataGridView1);
            dataGridView1.AllowUserToAddRows = false;
        }

        private void btnKitapListesiYazdır_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Yazdırma işleminde ilk başta yazıcıyı seçip, önizleme yaparak yazdır butonuna basınız.", "Yazdırma İşlemi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            DGVPrinter printer = new DGVPrinter();
            printer.PageNumbers = true;
            printer.PageNumberInHeader = true;
            printer.ShowTotalPageNumber = true;
            printer.PorportionalColumns = true;
            printer.HeaderCellAlignment = StringAlignment.Near;
            printer.CellAlignment = StringAlignment.Center;
            printer.TableAlignment = DGVPrinter.Alignment.Center;

            string okulAdi;
            try
            {
                string komut = "SELECT * FROM AYARLAR";
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                okulAdi = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi. Okul adı girilmemiş, Ayarlar Kısmından ekleyebilirsiniz.", "Öğrencinin Okuduğu Kitaplar Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            string ogrenciAd;
            string ogrenciSoyad;

            try
            {
                string komut2 = "SELECT AD,SOYAD FROM UYELER WHERE NO=" + numericOgrenciNo.Text;
                DataSet ds2 = Temel_Metodlar.SqlKomutCalistirSorgu(komut2);
                ogrenciAd = ds2.Tables[0].Rows[0].ItemArray[0].ToString();
                ogrenciSoyad = ds2.Tables[0].Rows[0].ItemArray[1].ToString();
                ds2.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi. Öğrenci No yanlış veya üye silinmiş.", "Öğrencinin Okuduğu Kitaplar Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (aralikBaslangic == "00000000" & aralikBitis == "99991230")
            {
                aralikBaslangic = "";
                aralikBitis = "";
            }
            else
            {
                aralikBaslangic = aralikBaslangic.Insert(4, "-").Insert(7, "-");
                aralikBitis = aralikBitis.Insert(4, "-").Insert(7, "-");
            }
            

            printer.Title = "Öğrencinin Okuduğu Kitapların Listesi";
            printer.SubTitle = aralikBaslangic + " / " + aralikBitis + " " + ogrenciAd + " " + ogrenciSoyad + "  ( " + dataGridView1.Rows.Count.ToString() + " Adet)";
            printer.Footer = okulAdi + " - " + DateTime.Now.ToLongDateString();
            printer.FooterAlignment = StringAlignment.Center;
            printer.PrintPreviewZoom = 0.7;
            printer.PrintPreviewDataGridView(dataGridView1);
        }

        private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Temel_Metodlar.Numaralandir(dataGridView1);
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            radioButonGridTemizle();
            if (radioButton4.Checked)
            {
                groupBox3.Enabled = true;
            }
            else
            {
                groupBox3.Enabled = false;
            }
        }

        private void numericOgrenciNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnOkunanKitaplariGetir.PerformClick();
            }
        }

        private void UCogrenciOkunanlar_Load(object sender, EventArgs e)
        {
            numericOgrenciNo.Text = "";
            numericOgrenciNo.Focus();
        }

        void radioButonGridTemizle()
        {
            dataGridView1.DataSource = null;
            btnKitapListesiYazdır.Enabled = false;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            radioButonGridTemizle();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            radioButonGridTemizle();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            radioButonGridTemizle();
        }
    }
}
