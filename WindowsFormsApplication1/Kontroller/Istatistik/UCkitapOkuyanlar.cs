﻿using System;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Data.SQLite;
using DGVPrinterHelper;

namespace ktProje.Kontroller.Istatistik
{
    public partial class UCkitapOkuyanlar : UserControl
    {
        public UCkitapOkuyanlar()
        {
            InitializeComponent();
        }

        string aralikBaslangic;
        string aralikBitis;



        private void btnKitapOkuyanGetir_Click(object sender, EventArgs e)
        {

            if (radioButton3.Checked)                   // tüm zamanlar
            {
                aralikBaslangic = "00000000";
                aralikBitis = "99991230";
            }
            if (radioButton1.Checked)                   // son bir ay
            {
                aralikBaslangic = DateTime.Today.AddMonths(-1).ToString("yyyyMMdd");
                aralikBitis = DateTime.Today.ToString("yyyyMMdd");
            }
            if (radioButton2.Checked)
            {
                aralikBaslangic = (DateTime.Today.Year - 1).ToString() + "0915";
                aralikBitis = DateTime.Today.ToString("yyyyMMdd");
            }
            if (radioButton4.Checked)
            {
                aralikBaslangic = dateTimePicker1.Value.ToString("yyyyMMdd");
                aralikBitis = dateTimePicker2.Value.ToString("yyyyMMdd");
            }


            if (radioOgrenci.Checked)
            {
                OgrenciBazindaSiralama();
                label4.Text = "*Öğrencinin okuduğu kitapları görmek için.\n             Sağ taraftan seçim yapınız.";
            }
            else
            {
                SinifBazindaSiralama();
                label4.Text = "*Sınıftaki kitap okuyanları görmek için.\n           Sağ taraftan seçim yapınız.";

            }

            Temel_Metodlar.Numaralandir(dataGridView1);
            dataGridView1.AllowUserToAddRows = false;
            dataGridView2.Visible = false;
            btnKitapListesiYazdır.Enabled = true;
        }

        private void OgrenciBazindaSiralama()
        {
            SQLiteCommand komut = new SQLiteCommand(@"SELECT count(OGRNO), OGRNO, u.AD,u.SOYAD,u.SINIF FROM ODUNC o INNER JOIN UYELER u   on o.OGRNO  = u.NO WHERE TESLIMDURUMU='GETİRDİ'  AND substr(OGRTESLIMTARIHI, -4)   ||  substr(OGRTESLIMTARIHI, instr('OGRTESLIMTARIHI', '')-6  ,-2)     ||  substr(OGRTESLIMTARIHI, instr('OGRTESLIMTARIHI', '')-9  ,-2) between @1 and @2   GROUP BY OGRNO  HAVING count(*) > 0 ORDER BY count(OGRNO)  DESC");


            komut.Parameters.AddWithValue("@1", aralikBaslangic);          //  başlangıç tarihi
            komut.Parameters.AddWithValue("@2", aralikBitis);          //  bitiş  tarihi

            try
            {
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                dataGridView1.DataSource = ds.Tables[0];
                komut.Dispose();
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi.", "Kitap Okuyanlar Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            dataGridView1.Columns[0].HeaderText = "Kitap Sayısı";
            dataGridView1.Columns[1].HeaderText = "Öğrenci No";
            dataGridView1.Columns[2].HeaderText = "Adı";
            dataGridView1.Columns[3].HeaderText = "Soyadı";
            dataGridView1.Columns[4].HeaderText = "Sınıfı";


            dataGridView1.Columns[0].Width = 55;
            dataGridView1.Columns[1].Width = 95;
            dataGridView1.Columns[2].Width = 100;
            dataGridView1.Columns[3].Width = 95;
            dataGridView1.Columns[4].Width = 60;

            dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.Columns[1].Visible = false;

        }

        private void SinifBazindaSiralama()
        {
            SQLiteCommand komut = new SQLiteCommand(@"SELECT count(SINIF ), u.SINIF FROM ODUNC o INNER JOIN UYELER u   on o.OGRNO  = u.NO WHERE TESLIMDURUMU='GETİRDİ'  AND substr(OGRTESLIMTARIHI, -4)   ||  substr(OGRTESLIMTARIHI, instr('OGRTESLIMTARIHI', '')-6  ,-2)     ||  substr(OGRTESLIMTARIHI, instr('OGRTESLIMTARIHI', '')-9  ,-2) between @1 and @2   GROUP BY SINIF HAVING count(*) > 0  ORDER BY count(SINIF)  DESC");


            komut.Parameters.AddWithValue("@1", aralikBaslangic);          //  başlangıç tarihi
            komut.Parameters.AddWithValue("@2", aralikBitis);          //  bitiş  tarihi

            try
            {
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                dataGridView1.DataSource = ds.Tables[0];
                komut.Dispose();
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi.", "Kitap Okuyanlar Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            dataGridView1.Columns[0].HeaderText = "Kitap Sayısı";
            dataGridView1.Columns[1].HeaderText = "Sınıf";

            dataGridView1.Columns[0].Width = 120;
            dataGridView1.Columns[1].Width = 120;
            dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void SinifOkuduguKitaplarDetayGetir(string sinif)
        {
            SQLiteCommand komut = new SQLiteCommand(@"SELECT count(OGRNO), u.AD, u.SOYAD FROM ODUNC o INNER JOIN UYELER u   on o.OGRNO = u.NO WHERE TESLIMDURUMU = 'GETİRDİ'  AND SINIF = @1   AND substr(OGRTESLIMTARIHI, -4) || substr(OGRTESLIMTARIHI, instr('OGRTESLIMTARIHI', '') - 6, -2) || substr(OGRTESLIMTARIHI, instr('OGRTESLIMTARIHI', '') - 9, -2) between @2 and @3   GROUP BY OGRNO  HAVING count(*) > 0 ORDER BY count(OGRNO)  DESC");

            komut.Parameters.AddWithValue("@1", sinif);          //  başlangıç tarihi
            komut.Parameters.AddWithValue("@2", aralikBaslangic);          //  başlangıç tarihi
            komut.Parameters.AddWithValue("@3", aralikBitis);          //  bitiş  tarihi

            try
            {
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                dataGridView2.DataSource = ds.Tables[0];
                komut.Dispose();
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi.", "Kitap Okuyanlar Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            dataGridView2.Columns[0].HeaderText = "Kitap Sayısı";
            dataGridView2.Columns[1].HeaderText = "Adı";
            dataGridView2.Columns[2].HeaderText = "Soyadı";

            dataGridView2.Columns[0].Width = 120;
            dataGridView2.Columns[1].Width = 110;
            dataGridView2.Columns[2].Width = 100;
            dataGridView2.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView2.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            Temel_Metodlar.Numaralandir(dataGridView2);
            dataGridView2.AllowUserToAddRows = false;
        }

        private void OgrenciOkuduguKitaplarDetayGetir(string ogrenciNo)
        {
            SQLiteCommand komut2 = new SQLiteCommand(@"SELECT k.ADI,o.OGRALISTARIHI,o.OGRTESLIMTARIHI,o.GECIKMEGUNU FROM ODUNC o INNER JOIN KITAPLAR k on o.BARKOD  = k.BARKOD WHERE OGRNO=@1 AND TESLIMDURUMU=@2 AND substr(OGRTESLIMTARIHI, -4)   ||  substr(OGRTESLIMTARIHI, instr('OGRTESLIMTARIHI', '')-6  ,-2)     ||  substr(OGRTESLIMTARIHI, instr('OGRTESLIMTARIHI', '')-9  ,-2) between @3 and @4 ");
            komut2.Parameters.AddWithValue("@1", ogrenciNo);
            komut2.Parameters.AddWithValue("@2", "GETİRDİ");

            komut2.Parameters.AddWithValue("@3", aralikBaslangic);          //  başlangıç tarihi
            komut2.Parameters.AddWithValue("@4", aralikBitis);          //  bitiş  tarihi

            try
            {
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut2);
                dataGridView2.DataSource = ds.Tables[0];
                komut2.Dispose();
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi.", "Kitap Okuyanlar Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            dataGridView2.Columns[0].HeaderText = "Kitap Adı";
            dataGridView2.Columns[1].HeaderText = "Alış Tarihi";
            dataGridView2.Columns[2].HeaderText = "Teslim Tarihi";
            dataGridView2.Columns[3].HeaderText = "Gecikme Günü";

            dataGridView2.Columns[0].Width = 175;
            dataGridView2.Columns[1].Width = 90;
            dataGridView2.Columns[2].Width = 90;
            dataGridView2.Columns[3].Width = 70;
            dataGridView2.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView2.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            Temel_Metodlar.Numaralandir(dataGridView2);
            dataGridView2.AllowUserToAddRows = false;
        }

        private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Temel_Metodlar.Numaralandir(dataGridView1);
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            radioButonGridTemizle();
            if (radioButton4.Checked)
            {
                groupBox3.Enabled = true;
            }
            else
            {
                groupBox3.Enabled = false;
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (radioOgrenci.Checked)
            {
                OgrenciOkuduguKitaplarDetayGetir(dataGridView1.CurrentRow.Cells[1].Value.ToString());
            }
            else
            {
                SinifOkuduguKitaplarDetayGetir(dataGridView1.CurrentRow.Cells[1].Value.ToString());
            }
            dataGridView2.Visible = true;
        }

        private void btnKitapListesiYazdır_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Yazdırma işleminde ilk başta yazıcıyı seçip, önizleme yaparak yazdır butonuna basınız.", "Yazdırma İşlemi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            DGVPrinter printer = new DGVPrinter();
            printer.Title = "Kitap Okuma Sayıları Listesi";
            printer.PageNumbers = true;
            printer.PageNumberInHeader = true;
            printer.ShowTotalPageNumber = true;
            printer.PorportionalColumns = true;
            printer.HeaderCellAlignment = StringAlignment.Near;
            printer.CellAlignment = StringAlignment.Center;
            printer.TableAlignment = DGVPrinter.Alignment.Center;

            string okulAdi;
            try
            {
                string komut = "SELECT * FROM AYARLAR";
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                okulAdi = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi. Okul adı girilmemiş, Ayarlar Kısmından ekleyebilirsiniz.", "Kitap Okuyanlar Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string altBaslik;
            if (aralikBaslangic == "00000000" & aralikBitis == "99991230")
            {
                aralikBaslangic = "";
                aralikBitis = "";
                altBaslik = "";
            }
            else
            {
                aralikBaslangic = aralikBaslangic.Insert(4, "-").Insert(7, "-");
                aralikBitis = aralikBitis.Insert(4, "-").Insert(7, "-");
                altBaslik = aralikBaslangic + " / " + aralikBitis + " Tarihleri Arasında";
            }

            printer.SubTitle = altBaslik;
            printer.Footer = okulAdi + " - " + DateTime.Now.ToLongDateString();
            printer.FooterAlignment = StringAlignment.Center;
            printer.PrintPreviewZoom = 0.7;
            printer.PrintPreviewDataGridView(dataGridView1);
        }

        void radioButonGridTemizle()
        {
            dataGridView1.DataSource = null;
            dataGridView2.DataSource = null;
            dataGridView2.Visible = false;
            btnKitapListesiYazdır.Enabled = false;
        }

        private void radioOgrenci_CheckedChanged(object sender, EventArgs e)
        {
            radioButonGridTemizle();
        }

        private void radioSinif_CheckedChanged(object sender, EventArgs e)
        {
            radioButonGridTemizle();
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            radioButonGridTemizle();

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            radioButonGridTemizle();

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            radioButonGridTemizle();

        }
    }
}
