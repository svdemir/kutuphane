﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;

namespace ktProje.Kontroller.Istatistik
{
    public partial class UCstatistikDuzenle : UserControl
    {
        public UCstatistikDuzenle()
        {
            InitializeComponent();
        }

        private void UCstatistikDuzenle_Load(object sender, EventArgs e)
        {

        }

        private void btnOgrNoGetir_Click(object sender, EventArgs e)
        {
            VerileriGetirNo();
        }



        private void VerileriGetirNo()
        {
            string komut = "SELECT o.ODUNCID,k.ADI,k.YAZAR, o.OGRNO,u.AD,u.SOYAD,u.SINIF, o.OGRALISTARIHI, o.TESLIMETMESIGEREKENTARIH FROM ODUNC o INNER JOIN UYELER u on o.OGRNO = u.NO INNER JOIN KITAPLAR k on o.BARKOD = k.BARKOD WHERE O.TESLIMDURUMU = 'GETİRDİ' AND o.OGRNO='" + numOduncOgrNo.Value.ToString() + "'";
            try
            {
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                dataGridView1.DataSource = ds.Tables[0];
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi.", "İstatistik Düzenle", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            dataGridView1.Columns[1].HeaderText = "Kitap Adı";
            dataGridView1.Columns[2].HeaderText = "Kitap Yazarı";
            dataGridView1.Columns[3].HeaderText = "Öğrenci No";
            dataGridView1.Columns[4].HeaderText = "Adı";
            dataGridView1.Columns[5].HeaderText = "Soyadı";
            dataGridView1.Columns[6].HeaderText = "Sınıfı";
            dataGridView1.Columns[7].HeaderText = "Alış Tarihi";
            dataGridView1.Columns[8].HeaderText = "Teslim Tarihi";

            dataGridView1.Columns[1].Width = 175;
            dataGridView1.Columns[2].Width = 110;
            dataGridView1.Columns[3].Width = 75;
            dataGridView1.Columns[4].Width = 100;
            dataGridView1.Columns[5].Width = 100;
            dataGridView1.Columns[6].Width = 65;
            dataGridView1.Columns[7].Width = 75;
            dataGridView1.Columns[8].Width = 115;

            dataGridView1.Columns[0].Visible = false;

            dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.AllowUserToAddRows = false;

            btnStatistikSil.Enabled = true;
        }

        private void btnStatistikSil_Click(object sender, EventArgs e)
        {

            if (dataGridView1.SelectedRows.Count > 0)
            {
                string oduncId = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                if (oduncId != "")
                {
                    DialogResult cevap = MessageBox.Show("Silme işlemini onaylıyor musunuz?", "Silme İşlemi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (cevap == DialogResult.Yes)
                    {
                        SQLiteCommand command = new SQLiteCommand("DELETE FROM ODUNC WHERE ODUNCID=@1");
                        command.Parameters.AddWithValue("@1", oduncId);
                        Temel_Metodlar.SqlKomutCalistir(command);
                        command.Dispose();
                        MessageBox.Show("İstatistik silme işlemi başarılı.", "İstatistik Düzenle", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        VerileriGetirNo();
                    }
                }
                else
                {
                    MessageBox.Show("İstatistik silme işlemi başarısız. ", "İstatistik Düzenle", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
