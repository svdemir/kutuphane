﻿using System;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Data.SQLite;
using DGVPrinterHelper;

namespace ktProje.Kontroller.Istatistik
{
    public partial class UCokunanKitaplar : UserControl
    {
        public UCokunanKitaplar()
        {
            InitializeComponent();
        }

        string aralikBaslangic;
        string aralikBitis;


        private void btnOkunanKitaplariGetir_Click(object sender, EventArgs e)
        {
            if (radioButton3.Checked)                   // tüm zamanlar
            {
                aralikBaslangic = "00000000";
                aralikBitis = "99991230";
            }
            if (radioButton1.Checked)                   // son bir ay
            {
                aralikBaslangic = DateTime.Today.AddMonths(-1).ToString("yyyyMMdd");
                aralikBitis = DateTime.Today.ToString("yyyyMMdd");
            }
            if (radioButton2.Checked)
            {
                aralikBaslangic = (DateTime.Today.Year - 1).ToString() + "0915";
                aralikBitis = DateTime.Today.ToString("yyyyMMdd");
            }
            if (radioButton4.Checked)
            {
                aralikBaslangic = dateTimePicker1.Value.ToString("yyyyMMdd");
                aralikBitis = dateTimePicker2.Value.ToString("yyyyMMdd");
            }


            if (radioOgrenci.Checked)
            {
                OkunanKitaplariOgrenciBazindaSiralama();
            }
            else
            {
                SinifBazlıSiralama();
            }



            Temel_Metodlar.Numaralandir(dataGridView1);
            dataGridView1.AllowUserToAddRows = false;
            dataGridView2.Visible = false;
            btnKitapListesiYazdır.Enabled = true;

        }


        private void OkunanKitaplariOgrenciBazindaSiralama()
        {
            SQLiteCommand komut = new SQLiteCommand(@"SELECT count(o.BARKOD) ,o.BARKOD,k.ADI FROM ODUNC o INNER JOIN KITAPLAR k   ON o.BARKOD = k.BARKOD WHERE TESLIMDURUMU='GETİRDİ'  AND substr(OGRTESLIMTARIHI, -4)   ||  substr(OGRTESLIMTARIHI, instr('OGRTESLIMTARIHI', '')-6  ,-2)     ||  substr(OGRTESLIMTARIHI, instr('OGRTESLIMTARIHI', '')-9  ,-2) between @1 and @2  GROUP BY o.BARKOD HAVING count(*) > 0 ORDER BY count(o.BARKOD)  DESC");


            komut.Parameters.AddWithValue("@1", aralikBaslangic);          //  başlangıç tarihi
            komut.Parameters.AddWithValue("@2", aralikBitis);          //  bitiş  tarihi

            try
            {
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                dataGridView1.DataSource = ds.Tables[0];
                komut.Dispose();
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi.", "Okunan Kitaplar Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            dataGridView1.RowHeadersVisible = true;


            dataGridView1.Columns[0].HeaderText = "Okunma Sayısı";
            dataGridView1.Columns[1].HeaderText = "Barkod";
            dataGridView1.Columns[2].HeaderText = "Kitap Adı";

            dataGridView1.Columns[0].Width = 70;
            dataGridView1.Columns[1].Width = 100;
            dataGridView1.Columns[2].Width = 160;
            dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.Columns[1].Visible = false;
        }

        private void OkunanKitapDetaylariGetir(string barkod)
        {
            SQLiteCommand komut2 = new SQLiteCommand(@"SELECT  o.BARKOD,u.SINIF,u.AD,u.SOYAD,o.OGRTESLIMTARIHI,o.GECIKMEGUNU FROM ODUNC o INNER JOIN UYELER u on o.OGRNO  = u.NO WHERE BARKOD=@1 AND TESLIMDURUMU=@2 AND substr(OGRTESLIMTARIHI, -4)   ||  substr(OGRTESLIMTARIHI, instr('OGRTESLIMTARIHI', '')-6  ,-2)     ||  substr(OGRTESLIMTARIHI, instr('OGRTESLIMTARIHI', '')-9  ,-2) between @3 and @4  ORDER BY OGRTESLIMTARIHI ASC ");

            komut2.Parameters.AddWithValue("@1", barkod);
            komut2.Parameters.AddWithValue("@2", "GETİRDİ");

            komut2.Parameters.AddWithValue("@3", aralikBaslangic);          //  başlangıç tarihi
            komut2.Parameters.AddWithValue("@4", aralikBitis);          //  bitiş  tarihi

            try
            {
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut2);
                dataGridView2.DataSource = ds.Tables[0];
                komut2.Dispose();
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi.", "Okunan Kitaplar Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            dataGridView2.Columns[0].HeaderText = "Barkod";
            dataGridView2.Columns[1].HeaderText = "Sınıfı";

            dataGridView2.Columns[2].HeaderText = "Adı";
            dataGridView2.Columns[3].HeaderText = "Soyadı";
            dataGridView2.Columns[4].HeaderText = "Teslim Tarihi";
            dataGridView2.Columns[5].HeaderText = "Gekikme Günü";

            dataGridView2.Columns[0].Width = 100;
            dataGridView2.Columns[1].Width = 50;
            dataGridView2.Columns[2].Width = 140;
            dataGridView2.Columns[3].Width = 90;
            dataGridView2.Columns[4].Width = 80;
            dataGridView2.Columns[5].Width = 60;

            dataGridView2.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView2.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView2.Columns[0].Visible = false;

            Temel_Metodlar.Numaralandir(dataGridView2);
            dataGridView2.AllowUserToAddRows = false;
        }

        private void SinifBazlıSiralama()
        {
            SQLiteCommand komut = new SQLiteCommand(@"SELECT MAX(sayi),t.BARKOD, k.ADI, t.SINIF  FROM 
                                                                           (SELECT count(BARKOD) AS sayi,
                                                                                  BARKOD,
                                                                                  SINIF
                                                                             FROM ODUNC o
                                                                                  INNER JOIN
                                                                                  UYELER u ON o.OGRNO = u.[NO]
                                                                            WHERE SINIF IN (
                                                                                      SELECT SINIF ADI
                                                                                        FROM SINIFLAR) AND TESLIMDURUMU = 'GETİRDİ' AND 
                    substr(OGRTESLIMTARIHI, -4) || substr(OGRTESLIMTARIHI, instr('OGRTESLIMTARIHI', '') - 6, -2) || substr(OGRTESLIMTARIHI, instr('OGRTESLIMTARIHI', '') - 9, -2) BETWEEN @1 AND @2
                                                                            GROUP BY SINIF,
                                                                                     barkod
                                                                           HAVING count( * ) > 0) t         
                                                                       INNER JOIN
                                                                       KITAPLAR k ON t.BARKOD = k.BARKOD
                                                                 GROUP BY SINIF;");


            komut.Parameters.AddWithValue("@1", aralikBaslangic);          //  başlangıç tarihi
            komut.Parameters.AddWithValue("@2", aralikBitis);          //  bitiş  tarihi

            try
            {
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                dataGridView1.DataSource = ds.Tables[0];
                komut.Dispose();
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi.", "Okunan Kitaplar Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            dataGridView1.RowHeadersVisible = false;

            dataGridView1.Columns[0].HeaderText = "Okunma Sayısı";
            dataGridView1.Columns[2].HeaderText = "Kitap Adı";
            dataGridView1.Columns[3].HeaderText = "Sınıf";

            dataGridView1.Columns[0].Width = 70;
            dataGridView1.Columns[1].Width = 100;
            dataGridView1.Columns[2].Width = 160;
            dataGridView1.Columns[3].Width = 100;

            dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.Columns[1].Visible = false;



        }

        private void SinifBazliSiralama(string barkod, string sinif)
        {
            SQLiteCommand komut2 = new SQLiteCommand(@"SELECT  o.BARKOD,u.SINIF,u.AD,u.SOYAD,o.OGRTESLIMTARIHI,o.GECIKMEGUNU FROM ODUNC o INNER JOIN UYELER u on o.OGRNO  = u.NO WHERE BARKOD=@1 AND u.SINIF=@5 AND TESLIMDURUMU=@2 AND substr(OGRTESLIMTARIHI, -4)   ||  substr(OGRTESLIMTARIHI, instr('OGRTESLIMTARIHI', '')-6  ,-2)     ||  substr(OGRTESLIMTARIHI, instr('OGRTESLIMTARIHI', '')-9  ,-2) between @3 and @4  ORDER BY OGRTESLIMTARIHI ASC ");

            komut2.Parameters.AddWithValue("@1", barkod);
            komut2.Parameters.AddWithValue("@2", "GETİRDİ");

            komut2.Parameters.AddWithValue("@3", aralikBaslangic);          //  başlangıç tarihi
            komut2.Parameters.AddWithValue("@4", aralikBitis);          //  bitiş  tarihi
            komut2.Parameters.AddWithValue("@5", sinif);

            try
            {
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut2);
                dataGridView2.DataSource = ds.Tables[0];
                komut2.Dispose();
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi.", "Okunan Kitaplar Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            dataGridView2.Columns[0].HeaderText = "Barkod";
            dataGridView2.Columns[1].HeaderText = "Sınıfı";

            dataGridView2.Columns[2].HeaderText = "Adı";
            dataGridView2.Columns[3].HeaderText = "Soyadı";
            dataGridView2.Columns[4].HeaderText = "Teslim Tarihi";
            dataGridView2.Columns[5].HeaderText = "Gekikme Günü";

            dataGridView2.Columns[0].Width = 100;
            dataGridView2.Columns[1].Width = 50;
            dataGridView2.Columns[2].Width = 140;
            dataGridView2.Columns[3].Width = 90;
            dataGridView2.Columns[4].Width = 80;
            dataGridView2.Columns[5].Width = 60;


            dataGridView2.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView2.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView2.Columns[0].Visible = false;

            Temel_Metodlar.Numaralandir(dataGridView2);
            dataGridView2.AllowUserToAddRows = false;            

        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            radioButonGridTemizle();
            if (radioButton4.Checked)
            {
                groupBox3.Enabled = true;
            }
            else
            {
                groupBox3.Enabled = false;
            }
        }

        private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Temel_Metodlar.Numaralandir(dataGridView1);
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (radioOgrenci.Checked)
            {
                OkunanKitapDetaylariGetir(dataGridView1.CurrentRow.Cells[1].Value.ToString());
            }
            else
            {
                SinifBazliSiralama(dataGridView1.CurrentRow.Cells[1].Value.ToString(), dataGridView1.CurrentRow.Cells[3].Value.ToString());
            }
            dataGridView2.Visible = true;
        }

        private void btnKitapListesiYazdır_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Yazdırma işleminde ilk başta yazıcıyı seçip, önizleme yaparak yazdır butonuna basınız.", "Yazdırma İşlemi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            DGVPrinter printer = new DGVPrinter();
            printer.Title = "Kitapların Okunma Sayıları Listesi";
            printer.PageNumbers = true;
            printer.PageNumberInHeader = true;
            printer.ShowTotalPageNumber = true;
            printer.PorportionalColumns = true;
            printer.HeaderCellAlignment = StringAlignment.Near;
            printer.CellAlignment = StringAlignment.Center;
            printer.TableAlignment = DGVPrinter.Alignment.Center;

            string okulAdi;
            try
            {
                string komut = "SELECT * FROM AYARLAR";
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                okulAdi = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi. Okul adı girilmemiş, Ayarlar Kısmından ekleyebilirsiniz.", "Okunan Kitaplar Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string altBaslik;
            if (aralikBaslangic == "00000000" & aralikBitis == "99991230")
            {
                aralikBaslangic = "";
                aralikBitis = "";
                altBaslik = "";
            }
            else
            {
                aralikBaslangic = aralikBaslangic.Insert(4, "-").Insert(7, "-");
                aralikBitis = aralikBitis.Insert(4, "-").Insert(7, "-");
                altBaslik = aralikBaslangic + " / " + aralikBitis + " Tarihleri Arasında";
            }

            printer.SubTitle = altBaslik;
            printer.Footer = okulAdi + " - " + DateTime.Now.ToLongDateString();
            printer.FooterAlignment = StringAlignment.Center;
            printer.PrintPreviewZoom = 0.7;
            printer.PrintPreviewDataGridView(dataGridView1);
        }

        void radioButonGridTemizle()
        {
            dataGridView1.DataSource = null;
            dataGridView2.DataSource = null;
            dataGridView2.Visible = false;
            btnKitapListesiYazdır.Enabled = false;
        }

        private void radioOgrenci_CheckedChanged(object sender, EventArgs e)
        {
            radioButonGridTemizle();
        }

        private void radioSinif_CheckedChanged(object sender, EventArgs e)
        {
            radioButonGridTemizle();
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            radioButonGridTemizle();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            radioButonGridTemizle();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            radioButonGridTemizle();
        }
    }
}
