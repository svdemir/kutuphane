﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SQLite;

namespace ktProje.Kontroller.Odunc
{
    public partial class UCoduncVer : UserControl
    {
        public UCoduncVer()
        {
            InitializeComponent();
        }

        private int KitapOduncSuresi()           //DB Ayarlar kısmından ödünç süresi verisi çekiliyor.
        {
            string komut = "SELECT * FROM AYARLAR";
            DataSet ds;
            int kitapOduncSuresi;
            try
            {
                ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                kitapOduncSuresi = Convert.ToInt32(ds.Tables[0].Rows[0].ItemArray[1]);
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("Kitap ödünç verme süresi hatası. Ayarlar kısmından düzenleyebilirsiniz.", "Ödünç Verme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw;
            }
            return kitapOduncSuresi;
        }

        private void btnUyeGetir_Click(object sender, EventArgs e)
        {
            SQLiteCommand command = new SQLiteCommand("SELECT * FROM UYELER WHERE NO=@1");                  //Sistem üye kontrolü yapılıyor.
            command.Parameters.AddWithValue("@1", numericUpDown2.Text);
            DataSet veriSeti = Temel_Metodlar.SqlKomutCalistirSorgu(command);
            if (veriSeti == null || veriSeti.Tables[0].Rows.Count == 0)
            {
                MessageBox.Show("Üye bulunamadı.", "Ödünç Verme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                uCuyeDetay1.Temizle();
                numericUpDown2.Text = null;
                return;
            }
            else
            {
                uCuyeDetay1.VeriEkle(veriSeti.Tables[0].Rows[0].ItemArray[0].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[1].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[2].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[3].ToString());
            }

            SQLiteCommand command2 = new SQLiteCommand("SELECT * FROM ODUNC WHERE OGRNO=@1 AND TESLIMDURUMU='GETİRMEDİ'");               //Üyede ödünç kitap olup olmadığı kontrol ediliyor.
            command2.Parameters.AddWithValue("@1", numericUpDown2.Text);
            DataSet veriSeti2 = Temel_Metodlar.SqlKomutCalistirSorgu(command2);

            SQLiteCommand command3 = new SQLiteCommand("SELECT ARKAPLANRESIM FROM AYARLAR");               //Üyede kaç adet ödünç kitap olduğu kontrol ediliyor. DB DE BOŞ ALAN OLMADIĞINDAN ARKAPLANRESIM ALANININ KULLANDIK.
            DataSet veriSeti3 = Temel_Metodlar.SqlKomutCalistirSorgu(command3);

            int oduncVerilecekKitapSayisi = Convert.ToInt32(veriSeti3.Tables[0].Rows[0].ItemArray[0]);

            if (veriSeti2.Tables[0].Rows.Count >= oduncVerilecekKitapSayisi)
            {
                for (int i = 0; i < veriSeti2.Tables[0].Rows.Count; i++)
                {
                    string uyedekiKitapBarkod = veriSeti2.Tables[0].Rows[i].ItemArray[1].ToString();
                    string sqlKitapDetayKomut = "SELECT ADI FROM KITAPLAR WHERE BARKOD=" + uyedekiKitapBarkod;
                    var kitapDetaySeti = Temel_Metodlar.SqlKomutCalistirSorgu(sqlKitapDetayKomut);
                    string kitapAdi = kitapDetaySeti.Tables[0].Rows[0].ItemArray[0].ToString();
                    MessageBox.Show("Üyede " + uyedekiKitapBarkod + " barkodlu " + kitapAdi + " adlı kitap bulunmaktadır.", "Ödünç Verme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    btnOduncVer.Enabled = false;
                }
            }
            else
            {
                btnOduncVer.Enabled = true;
            }

            command.Dispose();
            command2.Dispose();
            command3.Dispose();

            veriSeti.Dispose();
            veriSeti2.Dispose();
            veriSeti3.Dispose();

        }

        private void btnKitapGetir_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            SQLiteCommand command = new SQLiteCommand("SELECT BARKOD, ADI, YAZAR, TUR FROM KITAPLAR WHERE BARKOD=@1  OR ADI LIKE  @2  OR YAZAR  LIKE  @3");
            if (numericUpDown1.Text == "")
            {
                command.Parameters.AddWithValue("@1", null);
            }
            else
            {
                command.Parameters.AddWithValue("@1", numericUpDown1.Text);
            }

            if (textBox2.Text == "")
            {
                command.Parameters.AddWithValue("@2", "''");
            }
            else
            {
                command.Parameters.AddWithValue("@2", "%" + textBox2.Text.ToUpper() + "%");
            }

            if (textBox3.Text == "")
            {
                command.Parameters.AddWithValue("@3", "''");
            }
            else
            {
                command.Parameters.AddWithValue("@3", "%" + textBox3.Text.ToUpper() + "%");
            }


            DataSet veriSeti = Temel_Metodlar.SqlKomutCalistirSorgu(command);
            if (veriSeti == null || veriSeti.Tables[0].Rows.Count == 0)
            {
                MessageBox.Show("Kitap bulunamadı.", "Ödünç Verme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                dataGridView1.DataSource = veriSeti.Tables[0];

                dataGridView1.Columns[0].HeaderText = "Barkod";
                dataGridView1.Columns[1].HeaderText = "Kitap Adı";
                dataGridView1.Columns[2].HeaderText = "Yazarı";
                dataGridView1.Columns[3].HeaderText = "Türü";

                dataGridView1.Columns[0].Width = 80;
                dataGridView1.Columns[1].Width = 200;
                dataGridView1.Columns[2].Width = 200;
                dataGridView1.Columns[3].Width = 70;

            }
            dataGridView1.AllowUserToAddRows = false;
            command.Dispose();
            veriSeti.Dispose();
        }

        private void btnOduncVer_Click(object sender, EventArgs e)
        {
            if (numericUpDown2.Text == "")
            {
                MessageBox.Show("Üye no giriniz.", "Ödünç Verme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                uCuyeDetay1.Temizle();
                return;
            }


            int kitapOduncSuresi = KitapOduncSuresi();            //-------ayarlar tablosundan çekildi.

            string kitapAlanOgrenciNO = "";

            SQLiteCommand command = new SQLiteCommand("SELECT * FROM ODUNC WHERE BARKOD=@1");                   //Ödünç tablosu barkoda göre taranıyor,kitabı getirmeyen öğrencinin numarası alınıyor. 

            if (dataGridView1.CurrentRow != null)
            {
                command.Parameters.AddWithValue("@1", dataGridView1.CurrentRow.Cells[0].Value.ToString());          //kitap seçiniz uyarısı.

            }
            else
            {
                MessageBox.Show("Lütfen kitap seçiniz.", "Ödünç Verme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataSet veriSeti = Temel_Metodlar.SqlKomutCalistirSorgu(command);                                   //kitap hangi üyede kontrolu yapılıyor.
            if (veriSeti.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < veriSeti.Tables[0].Rows.Count; i++)
                {
                    if (veriSeti.Tables[0].Rows[i].ItemArray[7].ToString() == "GETİRMEDİ")
                    {
                        kitapAlanOgrenciNO = veriSeti.Tables[0].Rows[i].ItemArray[2].ToString();
                        string sqlUyeDetayKomut = "SELECT AD, SOYAD FROM UYELER WHERE NO=" + kitapAlanOgrenciNO;
                        var uyeDetaySeti = Temel_Metodlar.SqlKomutCalistirSorgu(sqlUyeDetayKomut);
                        string adSoyad = uyeDetaySeti.Tables[0].Rows[0].ItemArray[0].ToString() + " " + uyeDetaySeti.Tables[0].Rows[0].ItemArray[1].ToString();
                        MessageBox.Show("Kitap " + kitapAlanOgrenciNO + " numaralı " + adSoyad + " adlı üyede bulunmaktadır.", "Ödünç Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }

            if (kitapAlanOgrenciNO == "")
            {
                SQLiteCommand command2 = new SQLiteCommand("INSERT INTO ODUNC (BARKOD , OGRNO , OGRALISTARIHI , OGRTESLIMTARIHI  , TESLIMETMESIGEREKENTARIH , GECIKMEGUNU , TESLIMDURUMU) VALUES (@1,@2,@3,@4,@5,@6,@7)");
                command2.Parameters.AddWithValue("@1", dataGridView1.CurrentRow.Cells[0].Value.ToString());
                command2.Parameters.AddWithValue("@2", numericUpDown2.Text);
                command2.Parameters.AddWithValue("@3", DateTime.Today.ToString("dd/MM/yyyy"));
                command2.Parameters.AddWithValue("@4", "");                                                                     //update edilecek teslim almada. 
                command2.Parameters.AddWithValue("@5", DateTime.Today.AddDays(kitapOduncSuresi).ToString("dd/MM/yyyy"));
                command2.Parameters.AddWithValue("@6", "");
                command2.Parameters.AddWithValue("@7", "GETİRMEDİ");
                try
                {
                    Temel_Metodlar.SqlKomutCalistir(command2);
                    command2.Dispose();
                    MessageBox.Show("İşlem başarıyla gerçekleştirildi.", "Ödünç Verme İşlemi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception)
                {
                    MessageBox.Show("İşlem Hatası.", "Ödünç Verme İşlemi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            numericUpDown2.Text = null;
            uCuyeDetay1.Temizle();

            numericUpDown1.Text = null;
            textBox2.Text = "";
            textBox3.Text = "";
            dataGridView1.DataSource = null;
            dataGridView1.Refresh();

            btnOduncVer.Enabled = false;
            lblKitapGelecekTarih.Text = "Kitap iade tarihi    " + DateTime.Today.AddDays(kitapOduncSuresi).ToLongDateString();
            lblKitapGelecekTarih.Visible = true;

            command.Dispose();
            veriSeti.Dispose();
        }


        private void numericUpDown1_KeyDown(object sender, KeyEventArgs e)   //Enter tuşu ile onay sağlanıyor.
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnKitapGetir.PerformClick();
            }
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnKitapGetir.PerformClick();
            }
        }

        private void textBox3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnKitapGetir.PerformClick();
            }
        }

        private void UCoduncVer_Load(object sender, EventArgs e)
        {
            numericUpDown1.Text = "";
            numericUpDown2.Text = "";
            numericUpDown1.Focus();
            numericUpDown2.TextChanged += new EventHandler(numericUpDown2_TextChanged);
        }

        private void numericUpDown2_TextChanged(object sender, EventArgs e)
        {
            btnOduncVer.Enabled = false;
        }

        private void numericUpDown2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnUyeGetir.PerformClick();
            }
        }

    }
}
