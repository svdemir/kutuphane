﻿using System;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using DGVPrinterHelper;

namespace ktProje.Kontroller.Odunc
{
    public partial class UCuyedekiKitaplar : UserControl
    {
        public UCuyedekiKitaplar()
        {
            InitializeComponent();
        }

        private void UserControl1_Load(object sender, EventArgs e)
        {

        }

        private void btnKitapListesiGetir_Click(object sender, EventArgs e)
        {
            string komut = "SELECT o.BARKOD,k.ADI,k.YAZAR, o.OGRNO,u.AD,u.SOYAD,u.SINIF, o.OGRALISTARIHI, o.TESLIMETMESIGEREKENTARIH,o.ODUNCID,k.BASIMYILI FROM ODUNC o INNER JOIN UYELER u on o.OGRNO = u.NO INNER JOIN KITAPLAR k on o.BARKOD = k.BARKOD WHERE O.TESLIMDURUMU = 'GETİRMEDİ'";

            try
            {
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                dataGridView1.DataSource = ds.Tables[0];
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi.", "Üyelerdeki Kitaplar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            dataGridView1.Columns[0].HeaderText = "Barkod";
            dataGridView1.Columns[1].HeaderText = "Kitap Adı";
            dataGridView1.Columns[2].HeaderText = "Kitap Yazarı";
            dataGridView1.Columns[3].HeaderText = "Öğrenci No";
            dataGridView1.Columns[4].HeaderText = "Adı";
            dataGridView1.Columns[5].HeaderText = "Soyadı";
            dataGridView1.Columns[6].HeaderText = "Sınıfı";
            dataGridView1.Columns[7].HeaderText = "Alış Tarihi";
            dataGridView1.Columns[8].HeaderText = "İade Edilecek Tarihi";
            dataGridView1.Columns[9].HeaderText = "Kalan Gün";
            dataGridView1.Columns[10].HeaderText = "Gecikme Gün";


            dataGridView1.Columns[0].Width = 60;
            dataGridView1.Columns[1].Width = 175;
            dataGridView1.Columns[2].Width = 110;
            dataGridView1.Columns[3].Width = 75;
            dataGridView1.Columns[4].Width = 100;
            dataGridView1.Columns[5].Width = 90;
            dataGridView1.Columns[6].Width = 65;
            dataGridView1.Columns[7].Width = 75;
            dataGridView1.Columns[8].Width = 110;
            dataGridView1.Columns[9].Width = 60;
            dataGridView1.Columns[10].Width = 60;



            dataGridView1.Columns[0].HeaderCell.Style.BackColor = Color.RosyBrown;
            dataGridView1.Columns[1].HeaderCell.Style.BackColor = Color.RosyBrown;
            dataGridView1.Columns[2].HeaderCell.Style.BackColor = Color.RosyBrown;
            dataGridView1.Columns[3].HeaderCell.Style.BackColor = Color.SlateBlue;
            dataGridView1.Columns[4].HeaderCell.Style.BackColor = Color.SlateBlue;
            dataGridView1.Columns[5].HeaderCell.Style.BackColor = Color.SlateBlue;
            dataGridView1.Columns[6].HeaderCell.Style.BackColor = Color.SlateBlue;
            dataGridView1.Columns[7].HeaderCell.Style.BackColor = Color.SteelBlue;
            dataGridView1.Columns[8].HeaderCell.Style.BackColor = Color.SteelBlue;
            dataGridView1.Columns[9].HeaderCell.Style.BackColor = Color.SteelBlue;
            dataGridView1.Columns[10].HeaderCell.Style.BackColor = Color.SteelBlue;


            dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.AllowUserToAddRows = false;
            KalanGunHesapla(dataGridView1);
            dataGridView1.ClearSelection();

            lblKitapSayisi.Text = dataGridView1.RowCount.ToString();
            groupBox1.Visible = true;
            btnKitapListesiYazdır.Enabled = true;
        }

        private void btnKitapListesiYazdır_Click(object sender, EventArgs e)
        {

            MessageBox.Show("Yazdırma işleminde ilk başta yazıcıyı seçip, önizleme yaparak sayfa yönlendirme (Yatay önerilir.) , yazdırılacak sayfa sayısı ayarlarını düzenleyip, yazdır butonuna basınız.", "Yazdırma İşlemi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            DGVPrinter printer = new DGVPrinter();
            printer.Title = "Kütüphane Ödünç Kitap Listesi";
            printer.PageNumbers = true;
            printer.PageNumberInHeader = true;
            printer.ShowTotalPageNumber = true;
            printer.PorportionalColumns = true;
            printer.HeaderCellAlignment = StringAlignment.Near;
            printer.CellAlignment = StringAlignment.Center;
            printer.TableAlignment = DGVPrinter.Alignment.Center;

            yazdirDGV.DataSource = dataGridView1.DataSource;

            yazdirDGV.Columns[0].HeaderText = "Barkod";
            yazdirDGV.Columns[1].HeaderText = "Kitap Adı";
            yazdirDGV.Columns[2].HeaderText = "Kitap Yazarı";
            yazdirDGV.Columns[3].HeaderText = "Öğrenci No";
            yazdirDGV.Columns[4].HeaderText = "Adı";
            yazdirDGV.Columns[5].HeaderText = "Soyadı";
            yazdirDGV.Columns[6].HeaderText = "Sınıfı";
            yazdirDGV.Columns[7].HeaderText = "Alış Tarihi";
            yazdirDGV.Columns[8].HeaderText = "İade Edilecek Tarihi";
            yazdirDGV.Columns[9].HeaderText = "Kalan Gün";
            yazdirDGV.Columns[10].HeaderText = "Gecikme Gün";
            yazdirDGV.Columns[0].Width = 60;

            yazdirDGV.Columns[1].Width = 175;
            yazdirDGV.Columns[2].Width = 110;
            yazdirDGV.Columns[3].Width = 75;
            yazdirDGV.Columns[4].Width = 100;
            yazdirDGV.Columns[5].Width = 90;
            yazdirDGV.Columns[6].Width = 65;
            yazdirDGV.Columns[7].Width = 75;
            yazdirDGV.Columns[8].Width = 110;
            yazdirDGV.Columns[9].Width = 60;
            yazdirDGV.Columns[10].Width = 60;
            yazdirDGV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            string okulAdi;
            try
            {
                string komut = "SELECT * FROM AYARLAR";
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                okulAdi = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi. Okul adı girilmemiş, Ayarlar Kısmından ekleyebilirsiniz.", "Üyedeki Kitaplar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            printer.Footer = okulAdi + " - " + DateTime.Now.ToLongDateString();
            printer.FooterAlignment = StringAlignment.Center;

            printer.PrintPreviewZoom = 0.7;
            printer.PrintPreviewDataGridView(yazdirDGV);
        }

        private void KalanGunHesapla(DataGridView gridview)
        {
            for (int i = 0; i < gridview.RowCount; i++)
            {
                DateTime bugun = DateTime.Today;
                string iadeTarihi = gridview.Rows[i].Cells[8].Value.ToString();
                TimeSpan kalan = Convert.ToDateTime(iadeTarihi) - bugun;
                if (kalan.Days < 0)
                {
                    gridview.Rows[i].Cells[10].Value = Math.Abs(kalan.Days).ToString();
                    gridview.Rows[i].Cells[9].Value = DBNull.Value;                                   //kalan yok gecikme var.
                }
                else
                {
                    gridview.Rows[i].Cells[9].Value = kalan.Days.ToString();
                    gridview.Rows[i].Cells[10].Value = DBNull.Value;                                 //gecikme yok kalan gün var.
                }
            }
            GridRenklendir(dataGridView1);
        }

        private void GridRenklendir(DataGridView gridview)
        {
            for (int i = 0; i < gridview.RowCount; i++)
            {
                if (gridview.Rows[i].Cells[10].Value != DBNull.Value)
                {
                    gridview.Rows[i].DefaultCellStyle.BackColor = Color.IndianRed;
                }
                else
                {
                    gridview.Rows[i].DefaultCellStyle.BackColor = Color.LightSeaGreen;
                }
            }
            gridview.ClearSelection();
        }

        private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            GridRenklendir(dataGridView1);
        }

    }
}
