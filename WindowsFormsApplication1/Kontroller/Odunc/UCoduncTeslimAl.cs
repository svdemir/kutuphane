﻿using System;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Data.SQLite;

namespace ktProje.Kontroller.Odunc
{
    public partial class UCoduncTeslimAl : UserControl
    {
        public UCoduncTeslimAl()
        {
            InitializeComponent();
        }

        private void UCoduncTeslimAl_Load(object sender, EventArgs e)
        {

        }

        private void VerileriGetir()
        {
            string komut = "SELECT o.BARKOD,k.ADI,k.YAZAR, o.OGRNO,u.AD,u.SOYAD,u.SINIF, o.OGRALISTARIHI, o.TESLIMETMESIGEREKENTARIH,o.ODUNCID FROM ODUNC o INNER JOIN UYELER u on o.OGRNO = u.NO INNER JOIN KITAPLAR k on o.BARKOD = k.BARKOD WHERE O.TESLIMDURUMU = 'GETİRMEDİ'";
            try
            {
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                dataGridView1.DataSource = ds.Tables[0];
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi.", "Ödünç Teslim Alma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            dataGridView1.Columns[0].HeaderText = "Barkod";
            dataGridView1.Columns[1].HeaderText = "Kitap Adı";
            dataGridView1.Columns[2].HeaderText = "Kitap Yazarı";
            dataGridView1.Columns[3].HeaderText = "Öğrenci No";
            dataGridView1.Columns[4].HeaderText = "Adı";
            dataGridView1.Columns[5].HeaderText = "Soyadı";
            dataGridView1.Columns[6].HeaderText = "Sınıfı";
            dataGridView1.Columns[7].HeaderText = "Alış Tarihi";
            dataGridView1.Columns[8].HeaderText = "İade Edilecek Tarihi";
            dataGridView1.Columns[9].HeaderText = "Gecikme Gün";

            dataGridView1.Columns[0].Width = 60;
            dataGridView1.Columns[1].Width = 175;
            dataGridView1.Columns[2].Width = 110;
            dataGridView1.Columns[3].Width = 75;
            dataGridView1.Columns[4].Width = 100;
            dataGridView1.Columns[5].Width = 100;
            dataGridView1.Columns[6].Width = 65;
            dataGridView1.Columns[7].Width = 75;
            dataGridView1.Columns[8].Width = 115;
            dataGridView1.Columns[9].Width = 60;

            dataGridView1.Columns[0].HeaderCell.Style.BackColor = Color.RosyBrown;
            dataGridView1.Columns[1].HeaderCell.Style.BackColor = Color.RosyBrown;
            dataGridView1.Columns[2].HeaderCell.Style.BackColor = Color.RosyBrown;
            dataGridView1.Columns[3].HeaderCell.Style.BackColor = Color.SlateBlue;
            dataGridView1.Columns[4].HeaderCell.Style.BackColor = Color.SlateBlue;
            dataGridView1.Columns[5].HeaderCell.Style.BackColor = Color.SlateBlue;
            dataGridView1.Columns[6].HeaderCell.Style.BackColor = Color.SlateBlue;
            dataGridView1.Columns[7].HeaderCell.Style.BackColor = Color.SteelBlue;
            dataGridView1.Columns[8].HeaderCell.Style.BackColor = Color.SteelBlue;
            dataGridView1.Columns[9].HeaderCell.Style.BackColor = Color.SteelBlue;

            dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.AllowUserToAddRows = false;
            GecikmeGunHesapla(dataGridView1);

            groupBox1.Visible = true;
            if (dataGridView1.RowCount > 0)
            {
                lblKitapSayisi.Text = dataGridView1.RowCount.ToString();
            }
            else
            {
                lblKitapSayisi.Text = "0";
            }
        }

        private void VerileriGetirNo()
        {
            string komut = "SELECT o.BARKOD,k.ADI,k.YAZAR, o.OGRNO,u.AD,u.SOYAD,u.SINIF, o.OGRALISTARIHI, o.TESLIMETMESIGEREKENTARIH,o.ODUNCID FROM ODUNC o INNER JOIN UYELER u on o.OGRNO = u.NO INNER JOIN KITAPLAR k on o.BARKOD = k.BARKOD WHERE O.TESLIMDURUMU = 'GETİRMEDİ' AND o.OGRNO='" + numOduncOgrNo.Value.ToString() + "'";
            try
            {
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                dataGridView1.DataSource = ds.Tables[0];
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi.", "Ödünç Teslim Alma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            dataGridView1.Columns[0].HeaderText = "Barkod";
            dataGridView1.Columns[1].HeaderText = "Kitap Adı";
            dataGridView1.Columns[2].HeaderText = "Kitap Yazarı";
            dataGridView1.Columns[3].HeaderText = "Öğrenci No";
            dataGridView1.Columns[4].HeaderText = "Adı";
            dataGridView1.Columns[5].HeaderText = "Soyadı";
            dataGridView1.Columns[6].HeaderText = "Sınıfı";
            dataGridView1.Columns[7].HeaderText = "Alış Tarihi";
            dataGridView1.Columns[8].HeaderText = "İade Edilecek Tarihi";
            dataGridView1.Columns[9].HeaderText = "Gecikme Gün";

            dataGridView1.Columns[0].Width = 60;
            dataGridView1.Columns[1].Width = 175;
            dataGridView1.Columns[2].Width = 110;
            dataGridView1.Columns[3].Width = 75;
            dataGridView1.Columns[4].Width = 100;
            dataGridView1.Columns[5].Width = 100;
            dataGridView1.Columns[6].Width = 65;
            dataGridView1.Columns[7].Width = 75;
            dataGridView1.Columns[8].Width = 115;
            dataGridView1.Columns[9].Width = 60;

            dataGridView1.Columns[0].HeaderCell.Style.BackColor = Color.RosyBrown;
            dataGridView1.Columns[1].HeaderCell.Style.BackColor = Color.RosyBrown;
            dataGridView1.Columns[2].HeaderCell.Style.BackColor = Color.RosyBrown;
            dataGridView1.Columns[3].HeaderCell.Style.BackColor = Color.SlateBlue;
            dataGridView1.Columns[4].HeaderCell.Style.BackColor = Color.SlateBlue;
            dataGridView1.Columns[5].HeaderCell.Style.BackColor = Color.SlateBlue;
            dataGridView1.Columns[6].HeaderCell.Style.BackColor = Color.SlateBlue;
            dataGridView1.Columns[7].HeaderCell.Style.BackColor = Color.SteelBlue;
            dataGridView1.Columns[8].HeaderCell.Style.BackColor = Color.SteelBlue;
            dataGridView1.Columns[9].HeaderCell.Style.BackColor = Color.SteelBlue;

            dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.AllowUserToAddRows = false;
            GecikmeGunHesapla(dataGridView1);

            groupBox1.Visible = true;
            if (dataGridView1.RowCount > 0)
            {
                lblKitapSayisi.Text = dataGridView1.RowCount.ToString();
            }
            else
            {
                lblKitapSayisi.Text = "0";
            }
        }

        private void GecikmeGunHesapla(DataGridView gridview)
        {
            for (int i = 0; i < gridview.RowCount; i++)
            {
                string iadeTarihi = gridview.Rows[i].Cells[8].Value.ToString();
                DateTime bugun = DateTime.Today;
                TimeSpan gecikme = bugun - Convert.ToDateTime(iadeTarihi);
                if (gecikme.Days > 0)
                {
                    gridview.Rows[i].Cells[9].Value = gecikme.Days.ToString();
                }
                else
                {
                    gridview.Rows[i].Cells[9].Value = DBNull.Value;
                }
            }
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridViewRow row = dataGridView1.Rows[e.RowIndex];
            if (dataGridView1.Rows[e.RowIndex].Cells[9].Value != DBNull.Value)
            {
                row.DefaultCellStyle.BackColor = Color.LightCoral;
            }
            else
            {
                row.DefaultCellStyle.BackColor = Color.LightSeaGreen;
            }
        }

        private void TeslimAlmaIslemi()
        {
            string barkod = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            string ogrenciNO = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();

            string gecikmeGunu = "";
            if (dataGridView1.SelectedRows[0].Cells[9].Value != DBNull.Value)
            {
                gecikmeGunu = dataGridView1.SelectedRows[0].Cells[9].Value.ToString();
            }

            SQLiteCommand command = new SQLiteCommand("UPDATE ODUNC SET OGRTESLIMTARIHI = @1 , GECIKMEGUNU = @2 , TESLIMDURUMU = @3  WHERE BARKOD=@111 AND OGRNO=@222");
            command.Parameters.AddWithValue("@111", barkod);
            command.Parameters.AddWithValue("@222", ogrenciNO);
            command.Parameters.AddWithValue("@1", DateTime.Today.ToString("dd/MM/yyyy"));
            command.Parameters.AddWithValue("@2", gecikmeGunu);
            command.Parameters.AddWithValue("@3", "GETİRDİ");
            try
            {
                Temel_Metodlar.SqlKomutCalistir(command);
                command.Dispose();
                MessageBox.Show("İşlem başarıyla gerçekleştirildi.", "Ödünç Teslim Alma", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi.", "Ödünç Teslim Alma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            VerileriGetir();
        }

        private void btnTeslimAl_Click(object sender, EventArgs e)
        {
            if (dataGridView1.RowCount > 0)
            {
                DialogResult cevap = MessageBox.Show("Teslim Alma işlemini onaylıyor musunuz?", "Ödünç Teslim Alma", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (cevap == DialogResult.Yes)
                {
                    TeslimAlmaIslemi();
                }
            }
        }

        private void btnKitapListesiGetir_Click(object sender, EventArgs e)
        {
            VerileriGetir();
            if (dataGridView1.RowCount > 0)
            {
                btnTeslimAl.Enabled = true;
            }
        }

        private void btnOgrNoGetir_Click(object sender, EventArgs e)
        {
            VerileriGetirNo();
            if (dataGridView1.RowCount > 0)
            {
                btnTeslimAl.Enabled = true;
            }
        }

        private void numOduncOgrNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnOgrNoGetir.PerformClick();
            }
        }
    }
}
