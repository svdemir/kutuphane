﻿namespace ktProje.Kontroller.Uyeler
{
    partial class UCuyeEkle
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTemizle = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.uCuyeDetay1 = new ktProje.Kontroller.Uyeler.UCuyeDetay();
            this.btnKaydet = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnTemizle
            // 
            this.btnTemizle.Image = global::ktProje.Properties.Resources.reload;
            this.btnTemizle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTemizle.Location = new System.Drawing.Point(152, 240);
            this.btnTemizle.Name = "btnTemizle";
            this.btnTemizle.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnTemizle.Size = new System.Drawing.Size(119, 36);
            this.btnTemizle.TabIndex = 2;
            this.btnTemizle.Text = "  Temizle";
            this.btnTemizle.UseVisualStyleBackColor = true;
            this.btnTemizle.Click += new System.EventHandler(this.btnTemizle_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.uCuyeDetay1);
            this.groupBox1.Controls.Add(this.btnTemizle);
            this.groupBox1.Controls.Add(this.btnKaydet);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold);
            this.groupBox1.Location = new System.Drawing.Point(6, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(283, 290);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Üye Ekle";
            // 
            // uCuyeDetay1
            // 
            this.uCuyeDetay1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.uCuyeDetay1.Location = new System.Drawing.Point(7, 16);
            this.uCuyeDetay1.Margin = new System.Windows.Forms.Padding(4);
            this.uCuyeDetay1.Name = "uCuyeDetay1";
            this.uCuyeDetay1.Size = new System.Drawing.Size(272, 217);
            this.uCuyeDetay1.TabIndex = 0;
            // 
            // btnKaydet
            // 
            this.btnKaydet.Image = global::ktProje.Properties.Resources.add;
            this.btnKaydet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnKaydet.Location = new System.Drawing.Point(16, 240);
            this.btnKaydet.Name = "btnKaydet";
            this.btnKaydet.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnKaydet.Size = new System.Drawing.Size(118, 36);
            this.btnKaydet.TabIndex = 1;
            this.btnKaydet.Text = "   Kaydet";
            this.btnKaydet.UseVisualStyleBackColor = true;
            this.btnKaydet.Click += new System.EventHandler(this.btnKaydet_Click);
            // 
            // UCuyeEkle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "UCuyeEkle";
            this.Size = new System.Drawing.Size(296, 308);
            this.Load += new System.EventHandler(this.UCuyeEkle_Load);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private UCuyeDetay uCuyeDetay1;
        private System.Windows.Forms.Button btnKaydet;
        private System.Windows.Forms.Button btnTemizle;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
