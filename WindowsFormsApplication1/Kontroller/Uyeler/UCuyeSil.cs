﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SQLite;

namespace ktProje.Kontroller.Uyeler
{
    public partial class UCuyeSil : UserControl
    {
        public UCuyeSil()
        {
            InitializeComponent();
        }

        private void btnGetir_Click(object sender, EventArgs e)
        {
            SQLiteCommand command = new SQLiteCommand("SELECT * FROM UYELER WHERE NO=@1");
            command.Parameters.AddWithValue("@1", numericUpDown1.Text);
            DataSet veriSeti = Temel_Metodlar.SqlKomutCalistirSorgu(command);
            if (veriSeti == null || veriSeti.Tables[0].Rows.Count == 0)
            {
                MessageBox.Show("Aranılan üye bulunamadı.", "Üye İşlemleri Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                uCuyeDetay1.Temizle();
                numericUpDown1.Text = null;
                uCuyeDetay1.Enabled = false;
                return;
            }
            else
            {
                uCuyeDetay1.VeriEkle(veriSeti.Tables[0].Rows[0].ItemArray[0].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[1].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[2].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[3].ToString());
                btnSil.Enabled = true;
            }
            command.Dispose();
            veriSeti.Dispose();
        }

        private void btnSil_Click(object sender, EventArgs e)
        {
            SQLiteCommand commandOdunc = new SQLiteCommand("SELECT * FROM ODUNC WHERE OGRNO=@1 AND TESLIMDURUMU=@2");
            commandOdunc.Parameters.AddWithValue("@1", numericUpDown1.Text);
            commandOdunc.Parameters.AddWithValue("@2", "GETİRMEDİ");
            DataSet veriSetiOdunc;
            try
            {
                veriSetiOdunc = Temel_Metodlar.SqlKomutCalistirSorgu(commandOdunc);
            }
            catch (Exception)
            {
                MessageBox.Show("Veritabanı ödünç kitap getirme hatası.", "Üye Silme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int oduncDurumu = veriSetiOdunc.Tables[0].Rows.Count;

            if (oduncDurumu == 0)
            {
                DialogResult cevap = MessageBox.Show("Silme işlemini onaylıyor musunuz?", "Silme İşlemi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (cevap == DialogResult.Yes)
                {
                    SQLiteCommand command = new SQLiteCommand("DELETE FROM UYELER WHERE NO=@1");
                    command.Parameters.AddWithValue("@1", numericUpDown1.Text);
                    Temel_Metodlar.SqlKomutCalistir(command);
                    numericUpDown1.Text = null;
                    uCuyeDetay1.Temizle();
                    command.Dispose();
                    MessageBox.Show("Üye silme işlemi başarılı.", "Üye Silme", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Üye silme işlemi başarısız. " + "Üyede " + veriSetiOdunc.Tables[0].Rows[0].ItemArray[1].ToString() + " barkodlu kitap ödünç bulunmaktadır. Üyeyi sistemden silmek için Üye Toplu Düzenle menüsünü kullanabilirsiniz.", "Üye Silme", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            numericUpDown1.Text = null;
            uCuyeDetay1.Temizle();
            commandOdunc.Dispose();
            veriSetiOdunc.Dispose();
        }

        private void UCuyeSil_Load(object sender, EventArgs e)
        {
            numericUpDown1.Text = "";
            numericUpDown1.Focus();
        }

        private void numericUpDown1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnGetir.PerformClick();
            }
        }
    }
}
