﻿using System;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using DGVPrinterHelper;
using System.IO;
using System.Data.OleDb;

namespace ktProje.Kontroller.Uyeler
{
    public partial class UCuyeListele : UserControl
    {
        public UCuyeListele()
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
        }

        private void btnKitapListesiGetir_Click(object sender, EventArgs e)
        {
            string komut = "SELECT * FROM UYELER";
            try
            {
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                dataGridView1.DataSource = ds.Tables[0];
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("Üye listesi getirilemedi.", "Üye Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            for (int i = 0; i < dataGridView1.Columns.Count; i++)
            {
                if (dataGridView1.Columns.Count > 8)
                {
                    dataGridView1.Columns.RemoveAt(8);
                }
            }

            dataGridView1.Columns[0].HeaderText = "No";
            dataGridView1.Columns[1].HeaderText = "Adı";
            dataGridView1.Columns[2].HeaderText = "Soyadı";
            dataGridView1.Columns[3].HeaderText = "Sınıfı";

            dataGridView1.Columns[0].Width = 70;
            dataGridView1.Columns[1].Width = 250;
            dataGridView1.Columns[2].Width = 250;
            dataGridView1.Columns[3].Width = 70;

            dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Temel_Metodlar.Numaralandir(dataGridView1);
            dataGridView1.AllowUserToAddRows = false;
            btnKitapListesiGetir.Enabled = false;
            lblKitapSayisi.Text = dataGridView1.Rows.Count.ToString();
            groupBox1.Visible = true;
            btnKitapListesiYazdır.Enabled = true;
            btnExcelAktar.Enabled = true;
        }


        #region Excel Aktarma

        SaveFileDialog sfd;

        private void btnExcelAktar_Click(object sender, EventArgs e)
        {
            this.ParentForm.Enabled = false;
            ExcelYazdir();
        }

        private void ExcelYazdir()
        {

            sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xlsx)|*.xlsx";
            sfd.FileName = "uyelerExport.xlsx";

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    var a = ktProje.Properties.Resources.uyelerSablon;
                    FileStream fileStream = new FileStream(sfd.FileName, FileMode.Create);
                    fileStream.Write(a, 0, a.Length);
                    fileStream.Close();
                    fileStream.Dispose();
                    backgroundWorker1.RunWorkerAsync();
                }
                catch (Exception)
                {
                    MessageBox.Show("İşlem gerçekleştirilemedi. Dosya oluşturulurken hata oluştu tekrar deneyiniz.", "Üye Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

            }
            else
            {
            }
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                string connectionString2 = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + sfd.FileName + @"; Extended Properties=""Excel 12.0 Xml;HDR=YES""";
                OleDbConnection baglanti2 = new OleDbConnection(connectionString2);
                baglanti2.Open();

                for (int i = 0; i < dataGridView1.RowCount; i++)
                {
                    DataGridViewRow Row = dataGridView1.Rows[i];
                    OleDbCommand komut = new OleDbCommand("INSERT INTO [Sayfa1$] ([NO], [AD], [SOYAD], [SINIF]) VALUES ('" + Row.Cells[0].Value + "','" + Row.Cells[1].Value + "','" + Row.Cells[2].Value + "','" + Row.Cells[3].Value + "'" + ")", baglanti2);
                                        
                    try
                    {
                        komut.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                    }
                }
                baglanti2.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Aktarma işlemi hatası.", "Üye Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show("Aktarma işlemi tamamlandı.", "Üye Listesi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.ParentForm.Enabled = true;


        }

        private void backgroundWorker1_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
        }

        #endregion


        private void btnKitapListesiYazdır_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Yazdırma işleminde ilk başta yazıcıyı seçip, önizleme yaptıktan sonra, yazdır butonuna basınız.", "Yazdırma İşlemi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            DGVPrinter printer = new DGVPrinter();
            printer.Title = "Üye Listesi";
            printer.PageNumbers = true;
            printer.PageNumberInHeader = true;
            printer.ShowTotalPageNumber = true;
            printer.PorportionalColumns = true;
            printer.HeaderCellAlignment = StringAlignment.Near;

            string okulAdi;
            try
            {
                string komut = "SELECT * FROM AYARLAR";
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                okulAdi = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi. Okul adı girilmemiş, Ayarlar Kısmından ekleyebilirsiniz.", "Üye Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            printer.Footer = okulAdi + " - " + DateTime.Now.ToLongDateString();
            printer.FooterAlignment = StringAlignment.Center;
            printer.PrintPreviewZoom = 0.7;
            printer.PrintPreviewDataGridView(dataGridView1);
        }

        private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Temel_Metodlar.Numaralandir(dataGridView1);
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

    }
}
