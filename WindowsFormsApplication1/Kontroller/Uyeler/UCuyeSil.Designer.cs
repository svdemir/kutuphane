﻿namespace ktProje.Kontroller.Uyeler
{
    partial class UCuyeSil
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.uCuyeDetay1 = new ktProje.Kontroller.Uyeler.UCuyeDetay();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGetir = new System.Windows.Forms.Button();
            this.btnSil = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numericUpDown1);
            this.groupBox1.Controls.Add(this.uCuyeDetay1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnGetir);
            this.groupBox1.Controls.Add(this.btnSil);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(501, 309);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Üye Silme";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.numericUpDown1.Location = new System.Drawing.Point(60, 43);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            -1530494977,
            232830,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(134, 25);
            this.numericUpDown1.TabIndex = 4;
            this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numericUpDown1_KeyDown);
            // 
            // uCuyeDetay1
            // 
            this.uCuyeDetay1.Enabled = false;
            this.uCuyeDetay1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.uCuyeDetay1.Location = new System.Drawing.Point(117, 80);
            this.uCuyeDetay1.Margin = new System.Windows.Forms.Padding(5);
            this.uCuyeDetay1.Name = "uCuyeDetay1";
            this.uCuyeDetay1.Size = new System.Drawing.Size(277, 221);
            this.uCuyeDetay1.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(28, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "No";
            // 
            // btnGetir
            // 
            this.btnGetir.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold);
            this.btnGetir.Image = global::ktProje.Properties.Resources.list;
            this.btnGetir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGetir.Location = new System.Drawing.Point(225, 37);
            this.btnGetir.Name = "btnGetir";
            this.btnGetir.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnGetir.Size = new System.Drawing.Size(125, 33);
            this.btnGetir.TabIndex = 0;
            this.btnGetir.Text = "     Üye Getir";
            this.btnGetir.UseVisualStyleBackColor = true;
            this.btnGetir.Click += new System.EventHandler(this.btnGetir_Click);
            // 
            // btnSil
            // 
            this.btnSil.Enabled = false;
            this.btnSil.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold);
            this.btnSil.Image = global::ktProje.Properties.Resources.delete2;
            this.btnSil.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSil.Location = new System.Drawing.Point(367, 36);
            this.btnSil.Name = "btnSil";
            this.btnSil.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnSil.Size = new System.Drawing.Size(123, 34);
            this.btnSil.TabIndex = 0;
            this.btnSil.Text = "    Üye Sil";
            this.btnSil.UseVisualStyleBackColor = true;
            this.btnSil.Click += new System.EventHandler(this.btnSil_Click);
            // 
            // UCuyeSil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "UCuyeSil";
            this.Size = new System.Drawing.Size(517, 333);
            this.Load += new System.EventHandler(this.UCuyeSil_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private UCuyeDetay uCuyeDetay1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnGetir;
        private System.Windows.Forms.Button btnSil;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
    }
}
