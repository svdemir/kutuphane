﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SQLite;

namespace ktProje.Kontroller.Uyeler
{
    public partial class UCuyeDuzenle : UserControl
    {
        public UCuyeDuzenle()
        {
            InitializeComponent();
        }

        private void btnGetir_Click(object sender, EventArgs e)
        {
            SQLiteCommand command = new SQLiteCommand("SELECT * FROM UYELER WHERE NO=@1");
            command.Parameters.AddWithValue("@1", numericUpDown1.Text);
            DataSet veriSeti = Temel_Metodlar.SqlKomutCalistirSorgu(command);
            if (veriSeti == null || veriSeti.Tables[0].Rows.Count == 0)
            {
                MessageBox.Show("Aranılan üye bulunamadı.", "Veritabanı Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                uCuyeDetay1.Temizle();
                numericUpDown1.Text = null;
                uCuyeDetay1.Enabled = false;
                btnGuncelle.Enabled = false;
                btnTemizle.Enabled = false;
                return;
            }
            else
            {
                uCuyeDetay1.Temizle();
                uCuyeDetay1.VeriEkle(veriSeti.Tables[0].Rows[0].ItemArray[0].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[1].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[2].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[3].ToString());
                uCuyeDetay1.Enabled = true;
                btnGuncelle.Enabled = true;
                btnTemizle.Enabled = true;
            }
            command.Dispose();
            veriSeti.Dispose();
        }

        private void btnGuncelle_Click(object sender, EventArgs e)
        {
            var txtBoxVeriListesi = uCuyeDetay1.VeriDon();

            SQLiteCommand command = new SQLiteCommand("UPDATE UYELER SET NO = @1 , AD = @2 , SOYAD = @3 , SINIF = @4  WHERE NO=@111");
            command.Parameters.AddWithValue("@111", Convert.ToInt32(numericUpDown1.Text));
            command.Parameters.AddWithValue("@1", Convert.ToInt32(txtBoxVeriListesi[0].Trim().ToUpper()));
            command.Parameters.AddWithValue("@2", txtBoxVeriListesi[1].Trim().ToUpper());
            command.Parameters.AddWithValue("@3", txtBoxVeriListesi[2].Trim().ToUpper());
            command.Parameters.AddWithValue("@4", txtBoxVeriListesi[3].Trim().ToUpper());

            if (uCuyeDetay1.AlanKontrolu())
            {
                if (Temel_Metodlar.SqlKomutCalistir(command))
                {
                    MessageBox.Show("Üye düzenleme işlemi başarılı.", "Üye Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    uCuyeDetay1.Temizle();
                    numericUpDown1.Text = null;
                    uCuyeDetay1.Enabled = false;
                }
                else
                {
                    MessageBox.Show("Hata. Öğrenci numarasının farklı olmasına ve diğer alanlara dikkat ediniz.", "Üye Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Tüm alanları giriniz.", "Üye Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            command.Dispose();
        }

        private void btnTemizle_Click(object sender, EventArgs e)
        {
            uCuyeDetay1.Temizle();
        }

        private void UCuyeDuzenle_Load(object sender, EventArgs e)
        {
            uCuyeDetay1.SiniflariGetir();
            numericUpDown1.Text = "";
            numericUpDown1.Focus();
        }

        private void numericUpDown1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnGetir.PerformClick();
            }
        }
    }
}
