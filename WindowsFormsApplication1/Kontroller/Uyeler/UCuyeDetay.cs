﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace ktProje.Kontroller.Uyeler
{
    public partial class UCuyeDetay : UserControl
    {
        public UCuyeDetay()
        {
            InitializeComponent();
        }


        public void Temizle()
        {
            foreach (TextBox textbox in groupBox1.Controls.OfType<TextBox>())
            {
                textbox.Text = string.Empty;
            }
            numericUpDown1.Text = string.Empty;
            comboBox1.Text = string.Empty;
        }

        public void VeriEkle(string uyeNo, string uyeAdi, string uyeSoyadi, string uyeSinifi)
        {
            numericUpDown1.Text = uyeNo;
            textBox2.Text = uyeAdi;
            textBox3.Text = uyeSoyadi;
            comboBox1.Text = uyeSinifi;
        }

        public List<string> VeriDon()
        {
            List<string> veriListesi = new List<string>();
            veriListesi.Add(numericUpDown1.Text);
            veriListesi.Add(textBox2.Text);
            veriListesi.Add(textBox3.Text);
            veriListesi.Add(comboBox1.Text);
            return veriListesi;
        }

        public void SiniflariGetir()
        {
            try
            {
                string siniflar = "SELECT * FROM SINIFLAR ORDER BY SINIF ASC";
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(siniflar);
                comboBox1.DataSource = ds.Tables[0];
                comboBox1.DisplayMember = "SINIF";
                comboBox1.ValueMember = "SINIF";
                comboBox1.SelectedIndex = -1;
                ds.Dispose();
            }
            catch (System.Exception)
            {
                MessageBox.Show("Sınıf verileri getirme işlemi gerçekleştirilemedi.", "Üye Detayları", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        public bool AlanKontrolu()
        {
            if (numericUpDown1.Text == "" || textBox2.Text == "" || textBox3.Text =="" || comboBox1.Text =="")
            {               
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
