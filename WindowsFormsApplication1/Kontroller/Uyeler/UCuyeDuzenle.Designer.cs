﻿namespace ktProje.Kontroller.Uyeler
{
    partial class UCuyeDuzenle
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.uCuyeDetay1 = new ktProje.Kontroller.Uyeler.UCuyeDetay();
            this.label2 = new System.Windows.Forms.Label();
            this.btnTemizle = new System.Windows.Forms.Button();
            this.btnGetir = new System.Windows.Forms.Button();
            this.btnGuncelle = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numericUpDown1);
            this.groupBox1.Controls.Add(this.uCuyeDetay1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnTemizle);
            this.groupBox1.Controls.Add(this.btnGetir);
            this.groupBox1.Controls.Add(this.btnGuncelle);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(465, 311);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Üye Bilgisi Düzenle";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.numericUpDown1.Location = new System.Drawing.Point(67, 41);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 25);
            this.numericUpDown1.TabIndex = 9;
            this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numericUpDown1_KeyDown);
            // 
            // uCuyeDetay1
            // 
            this.uCuyeDetay1.Enabled = false;
            this.uCuyeDetay1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.uCuyeDetay1.Location = new System.Drawing.Point(29, 73);
            this.uCuyeDetay1.Margin = new System.Windows.Forms.Padding(5);
            this.uCuyeDetay1.Name = "uCuyeDetay1";
            this.uCuyeDetay1.Size = new System.Drawing.Size(281, 221);
            this.uCuyeDetay1.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(35, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "No";
            // 
            // btnTemizle
            // 
            this.btnTemizle.Enabled = false;
            this.btnTemizle.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnTemizle.Image = global::ktProje.Properties.Resources.reload1;
            this.btnTemizle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTemizle.Location = new System.Drawing.Point(318, 185);
            this.btnTemizle.Name = "btnTemizle";
            this.btnTemizle.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnTemizle.Size = new System.Drawing.Size(126, 58);
            this.btnTemizle.TabIndex = 2;
            this.btnTemizle.Text = "    Temizle";
            this.btnTemizle.UseVisualStyleBackColor = true;
            this.btnTemizle.Click += new System.EventHandler(this.btnTemizle_Click);
            // 
            // btnGetir
            // 
            this.btnGetir.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnGetir.Image = global::ktProje.Properties.Resources.uye2;
            this.btnGetir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGetir.Location = new System.Drawing.Point(194, 35);
            this.btnGetir.Name = "btnGetir";
            this.btnGetir.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnGetir.Size = new System.Drawing.Size(116, 36);
            this.btnGetir.TabIndex = 1;
            this.btnGetir.Text = "     Üye Getir";
            this.btnGetir.UseVisualStyleBackColor = true;
            this.btnGetir.Click += new System.EventHandler(this.btnGetir_Click);
            // 
            // btnGuncelle
            // 
            this.btnGuncelle.Enabled = false;
            this.btnGuncelle.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnGuncelle.Image = global::ktProje.Properties.Resources.computing;
            this.btnGuncelle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuncelle.Location = new System.Drawing.Point(318, 101);
            this.btnGuncelle.Name = "btnGuncelle";
            this.btnGuncelle.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnGuncelle.Size = new System.Drawing.Size(126, 58);
            this.btnGuncelle.TabIndex = 1;
            this.btnGuncelle.Text = "     Güncelle";
            this.btnGuncelle.UseVisualStyleBackColor = true;
            this.btnGuncelle.Click += new System.EventHandler(this.btnGuncelle_Click);
            // 
            // UCuyeDuzenle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "UCuyeDuzenle";
            this.Size = new System.Drawing.Size(476, 325);
            this.Load += new System.EventHandler(this.UCuyeDuzenle_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private UCuyeDetay uCuyeDetay1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnTemizle;
        public System.Windows.Forms.Button btnGetir;
        private System.Windows.Forms.Button btnGuncelle;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
    }
}
