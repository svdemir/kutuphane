﻿using System;
using System.Windows.Forms;
using System.Data.SQLite;

namespace ktProje.Kontroller.Uyeler
{
    public partial class UCuyeEkle : UserControl
    {
        public UCuyeEkle()
        {
            InitializeComponent();
        }

        private void btnKaydet_Click(object sender, EventArgs e)
        {
            var txtBoxVeriListesi = uCuyeDetay1.VeriDon();
            if (uCuyeDetay1.AlanKontrolu())
            {
                SQLiteCommand command = new SQLiteCommand("INSERT INTO UYELER (NO , AD , SOYAD, SINIF) VALUES (@1,@2,@3,@4)");
                for (int i = 0; i < txtBoxVeriListesi.Count; i++)
                {
                    command.Parameters.AddWithValue("@" + (i + 1).ToString(), txtBoxVeriListesi[i].Trim().ToUpper());
                }

                if (Temel_Metodlar.SqlKomutCalistir(command))
                {
                    MessageBox.Show("Üye kayıt işlemi başarılı.", "Üye Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Hata. Öğrenci numarasının farklı olmasına ve diğer alanlara dikkat ediniz.", "Üye Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                uCuyeDetay1.Temizle();
                command.Dispose();
            }
            else
            {
                MessageBox.Show("Tüm alanları giriniz.", "Üye Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

           
        }

        private void btnTemizle_Click(object sender, EventArgs e)
        {
            uCuyeDetay1.Temizle();
        }

        private void UCuyeEkle_Load(object sender, EventArgs e)
        {
            uCuyeDetay1.Focus();
            uCuyeDetay1.Temizle();
            uCuyeDetay1.SiniflariGetir();
        }
    }
}
