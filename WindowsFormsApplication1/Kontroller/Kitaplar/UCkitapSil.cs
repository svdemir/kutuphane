﻿using System;
using System.Data;
using System.Data.SQLite;
using System.Windows.Forms;

namespace ktProje.Kontroller.Kitaplar
{
    public partial class UCkitapSil : UserControl
    {
        public UCkitapSil()
        {
            InitializeComponent();
        }

        private void btnGetir_Click(object sender, EventArgs e)
        {
            SQLiteCommand command = new SQLiteCommand("SELECT * FROM KITAPLAR WHERE BARKOD=@1");
            command.Parameters.AddWithValue("@1", numericUpDown1.Text);
            DataSet veriSeti;
            try
            {
                veriSeti = Temel_Metodlar.SqlKomutCalistirSorgu(command);
                command.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("Veritabanı kitap getirme hatası.", "Kitap Silme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (veriSeti == null || veriSeti.Tables[0].Rows.Count == 0)
            {
                MessageBox.Show("Aranılan kitap bulunamadı.", "Kitap Silme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                uCkitapDetay1.Temizle();
                numericUpDown1.Text = null;
                return;
            }
            else
            {
                uCkitapDetay1.VeriEkle(veriSeti.Tables[0].Rows[0].ItemArray[1].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[2].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[3].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[6].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[9].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[10].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[5].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[4].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[11].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[7].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[8].ToString());
                btnSil.Enabled = true;
            }
        }

        private void btnSil_Click(object sender, EventArgs e)                           //Kitap ödünç verilmemişse silme işlemi yapılıyor.
        {
            SQLiteCommand commandOdunc = new SQLiteCommand("SELECT * FROM ODUNC WHERE BARKOD=@1 AND TESLIMDURUMU=@2");
            commandOdunc.Parameters.AddWithValue("@1", numericUpDown1.Text);
            commandOdunc.Parameters.AddWithValue("@2", "GETİRMEDİ");
            DataSet veriSetiOdunc;
            try
            {
                veriSetiOdunc = Temel_Metodlar.SqlKomutCalistirSorgu(commandOdunc);
                commandOdunc.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("Veritabanı ödünç kitap getirme hatası.", "Kitap Silme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int oduncDurumu = veriSetiOdunc.Tables[0].Rows.Count;

            if (oduncDurumu == 0)
            {
                DialogResult cevap = MessageBox.Show("Silme işlemini onaylıyor musunuz?", "Silme İşlemi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (cevap == DialogResult.Yes)
                {
                    SQLiteCommand command = new SQLiteCommand("DELETE FROM KITAPLAR WHERE BARKOD=@1");
                    command.Parameters.AddWithValue("@1", numericUpDown1.Text);
                    Temel_Metodlar.SqlKomutCalistir(command);
                    command.Dispose();
                    numericUpDown1.Text = null;
                    uCkitapDetay1.Temizle();
                    MessageBox.Show("Kitap silme işlemi başarılı.", "Kitap Silme", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Kitap silme işlemi başarısız. " + "Kitap " + veriSetiOdunc.Tables[0].Rows[0].ItemArray[2].ToString() + " numaralı üyede ödünç bulunmaktadır. Kitabı sistemden silmek için Kitap Toplu Düzenle menüsünü kullanabilirsiniz.", "Kitap Silme", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            numericUpDown1.Text = null;
            uCkitapDetay1.Temizle();
            veriSetiOdunc.Dispose();
        }


        private void UCkitapSil_Load(object sender, EventArgs e)
        {
            numericUpDown1.Text = "";
            uCkitapDetay1.Temizle();
            numericUpDown1.Focus();
        }

        private void numericUpDown1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnGetir.PerformClick();
            }
        }
    }
}
