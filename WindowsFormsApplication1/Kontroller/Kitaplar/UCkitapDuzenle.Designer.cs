﻿namespace ktProje.Kontroller.Kitaplar
{
    partial class UCkitapDuzenle
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.btnTemizle = new System.Windows.Forms.Button();
            this.btnKitapGetir = new System.Windows.Forms.Button();
            this.btnGuncelle = new System.Windows.Forms.Button();
            this.uCkitapDetay1 = new ktProje.Kontroller.Kitaplar.UCkitapDetay();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numericUpDown1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnTemizle);
            this.groupBox1.Controls.Add(this.btnKitapGetir);
            this.groupBox1.Controls.Add(this.btnGuncelle);
            this.groupBox1.Controls.Add(this.uCkitapDetay1);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(820, 338);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Kitap Bilgisi Düzenle";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(173, 32);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(125, 29);
            this.numericUpDown1.TabIndex = 4;
            this.numericUpDown1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numericUpDown1_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(85, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Barkod No";
            // 
            // btnTemizle
            // 
            this.btnTemizle.Enabled = false;
            this.btnTemizle.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnTemizle.Image = global::ktProje.Properties.Resources.reload;
            this.btnTemizle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTemizle.Location = new System.Drawing.Point(660, 32);
            this.btnTemizle.Name = "btnTemizle";
            this.btnTemizle.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnTemizle.Size = new System.Drawing.Size(129, 36);
            this.btnTemizle.TabIndex = 3;
            this.btnTemizle.Text = "Temizle";
            this.btnTemizle.UseVisualStyleBackColor = true;
            this.btnTemizle.Click += new System.EventHandler(this.btnTemizle_Click);
            // 
            // btnKitapGetir
            // 
            this.btnKitapGetir.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnKitapGetir.Image = global::ktProje.Properties.Resources.layers;
            this.btnKitapGetir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnKitapGetir.Location = new System.Drawing.Point(342, 32);
            this.btnKitapGetir.Name = "btnKitapGetir";
            this.btnKitapGetir.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnKitapGetir.Size = new System.Drawing.Size(126, 36);
            this.btnKitapGetir.TabIndex = 1;
            this.btnKitapGetir.Text = "     Kitap Getir";
            this.btnKitapGetir.UseVisualStyleBackColor = true;
            this.btnKitapGetir.Click += new System.EventHandler(this.btnKitapGetir_Click);
            // 
            // btnGuncelle
            // 
            this.btnGuncelle.Enabled = false;
            this.btnGuncelle.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnGuncelle.Image = global::ktProje.Properties.Resources.edit;
            this.btnGuncelle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuncelle.Location = new System.Drawing.Point(505, 32);
            this.btnGuncelle.Name = "btnGuncelle";
            this.btnGuncelle.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnGuncelle.Size = new System.Drawing.Size(126, 36);
            this.btnGuncelle.TabIndex = 2;
            this.btnGuncelle.Text = "  Güncelle";
            this.btnGuncelle.UseVisualStyleBackColor = true;
            this.btnGuncelle.Click += new System.EventHandler(this.btnGuncelle_Click);
            // 
            // uCkitapDetay1
            // 
            this.uCkitapDetay1.Enabled = false;
            this.uCkitapDetay1.Font = new System.Drawing.Font("Segoe UI Black", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.uCkitapDetay1.Location = new System.Drawing.Point(10, 72);
            this.uCkitapDetay1.Margin = new System.Windows.Forms.Padding(4);
            this.uCkitapDetay1.Name = "uCkitapDetay1";
            this.uCkitapDetay1.Size = new System.Drawing.Size(803, 258);
            this.uCkitapDetay1.TabIndex = 0;
            // 
            // UCkitapDuzenle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "UCkitapDuzenle";
            this.Size = new System.Drawing.Size(828, 347);
            this.Load += new System.EventHandler(this.UCkitapDuzenle_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private UCkitapDetay uCkitapDetay1;
        private System.Windows.Forms.Button btnGuncelle;
        private System.Windows.Forms.Button btnTemizle;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.Button btnKitapGetir;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
    }
}
