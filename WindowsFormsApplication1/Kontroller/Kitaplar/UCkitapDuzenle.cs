﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SQLite;

namespace ktProje.Kontroller.Kitaplar
{
    public partial class UCkitapDuzenle : UserControl
    {
        public UCkitapDuzenle()
        {
            InitializeComponent();
        }

        private void btnKitapGetir_Click(object sender, EventArgs e)
        {
            SQLiteCommand command = new SQLiteCommand("SELECT * FROM KITAPLAR WHERE BARKOD=@1");
            command.Parameters.AddWithValue("@1", numericUpDown1.Text);
            DataSet veriSeti;
            try
            {
                veriSeti = Temel_Metodlar.SqlKomutCalistirSorgu(command);
            }
            catch (Exception)
            {
                MessageBox.Show("Kitap veritabanı getirme hatası.", "Kitap Düzenle", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (veriSeti==null || veriSeti.Tables[0].Rows.Count == 0)
            {
                MessageBox.Show("Aranılan kitap bulunamadı.", "Kitap Düzenle", MessageBoxButtons.OK, MessageBoxIcon.Error);
                uCkitapDetay1.Temizle();
                numericUpDown1.Text = null;
                uCkitapDetay1.Enabled = false;
                btnGuncelle.Enabled = false;
                btnTemizle.Enabled = false;
                return;
            }
            else
            {
                uCkitapDetay1.DbVerileriGetir();
                uCkitapDetay1.VeriEkle(veriSeti.Tables[0].Rows[0].ItemArray[1].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[2].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[3].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[6].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[9].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[10].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[5].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[4].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[11].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[7].ToString(), veriSeti.Tables[0].Rows[0].ItemArray[8].ToString());
                uCkitapDetay1.Enabled = true;
                btnGuncelle.Enabled = true;
                btnTemizle.Enabled = true;
            }
        }

        private void btnTemizle_Click(object sender, EventArgs e)
        {
            uCkitapDetay1.Temizle();
        }

        private void btnGuncelle_Click(object sender, EventArgs e)
        {
            try
            {
                var txtBoxVeriListesi = uCkitapDetay1.VeriDon();
                uCkitapDetay1.DbVerileriGetir();

                SQLiteCommand command = new SQLiteCommand("UPDATE KITAPLAR SET ADI = @1 , YAZAR = @2 , TUR = @3 , DNO = @4 , BARKOD = @5 , YAYINEVI = @6 , DOLAP = @7 , RAF = @8 , SAYFASAYISI = @9 , BASIMYILI = @10 , ISBNNO = @11 WHERE BARKOD=@111");

                command.Parameters.AddWithValue("@111", Convert.ToInt32(numericUpDown1.Text));

                command.Parameters.AddWithValue("@1", txtBoxVeriListesi[0].Trim().ToUpper());
                command.Parameters.AddWithValue("@2", txtBoxVeriListesi[1].Trim().ToUpper());
                command.Parameters.AddWithValue("@3", txtBoxVeriListesi[2].Trim().ToUpper());
                if (!string.IsNullOrEmpty(txtBoxVeriListesi[7]))
                {
                    command.Parameters.AddWithValue("@4", Convert.ToInt32(txtBoxVeriListesi[7].Trim().ToUpper()));
                }
                else
                {
                    command.Parameters.AddWithValue("@4", 0);
                }
                if (!string.IsNullOrEmpty(txtBoxVeriListesi[6]))
                {
                    command.Parameters.AddWithValue("@5", Convert.ToInt32(txtBoxVeriListesi[6].Trim().ToUpper()));
                }
                else
                {
                    command.Parameters.AddWithValue("@5", 0);
                }
                command.Parameters.AddWithValue("@6", txtBoxVeriListesi[3].Trim().ToUpper());
                command.Parameters.AddWithValue("@7", txtBoxVeriListesi[9].Trim().ToUpper());
                command.Parameters.AddWithValue("@8", txtBoxVeriListesi[10].Trim().ToUpper());
                if (!string.IsNullOrEmpty(txtBoxVeriListesi[4]))
                {
                    command.Parameters.AddWithValue("@9", Convert.ToInt32(txtBoxVeriListesi[4].Trim().ToUpper()));
                }
                else
                {
                    command.Parameters.AddWithValue("@9", 0);
                }
                if (!string.IsNullOrEmpty(txtBoxVeriListesi[5]))
                {
                    command.Parameters.AddWithValue("@10", Convert.ToInt32(txtBoxVeriListesi[5].Trim().ToUpper()));
                }
                else
                {
                    command.Parameters.AddWithValue("@10", 0);
                }
                command.Parameters.AddWithValue("@11", txtBoxVeriListesi[8].Trim().ToUpper());
                Temel_Metodlar.SqlKomutCalistir(command);
                command.Dispose();

                uCkitapDetay1.Temizle();
                numericUpDown1.Text = null;
                uCkitapDetay1.Enabled = false;
                MessageBox.Show("Güncelleme işlemi başarılı.", "Kitap Düzenle", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {
                MessageBox.Show("Güncelleme işlemi başarısız.", "Kitap Düzenle", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void UCkitapDuzenle_Load(object sender, EventArgs e)
        {
            numericUpDown1.Focus();
            numericUpDown1.Text = "";
            uCkitapDetay1.Temizle();
        }

        private void numericUpDown1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnKitapGetir.PerformClick();
            }
        }
    }
}
