﻿using System;
using System.Windows.Forms;
using System.Data.SQLite;

namespace ktProje.Kontroller.Kitaplar
{
    public partial class UCkitapEkle : UserControl
    {
        public UCkitapEkle()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var txtBoxVeriListesi = uCkitapDetay1.VeriDon();

            if (txtBoxVeriListesi[7] == "")                     //Demirbaş no boş geçilirse, barkod no ile eşleştiriliyor.
            {
                txtBoxVeriListesi[7] = txtBoxVeriListesi[6];
            }

            if (txtBoxVeriListesi[0] != "" && txtBoxVeriListesi[6] != "" && txtBoxVeriListesi[1] != "")
            {
                SQLiteCommand command = new SQLiteCommand("INSERT INTO KITAPLAR (ADI , YAZAR , TUR , DNO , BARKOD , YAYINEVI , DOLAP , RAF , SAYFASAYISI , BASIMYILI , ISBNNO ) VALUES (@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11)");

                command.Parameters.AddWithValue("@1", txtBoxVeriListesi[0].Trim().ToUpper());
                command.Parameters.AddWithValue("@2", txtBoxVeriListesi[1].Trim().ToUpper());
                command.Parameters.AddWithValue("@3", txtBoxVeriListesi[2].Trim().ToUpper());
                command.Parameters.AddWithValue("@4", txtBoxVeriListesi[7].Trim().ToUpper());
                command.Parameters.AddWithValue("@5", txtBoxVeriListesi[6].Trim().ToUpper());
                command.Parameters.AddWithValue("@6", txtBoxVeriListesi[3].Trim().ToUpper());
                command.Parameters.AddWithValue("@7", txtBoxVeriListesi[9].Trim().ToUpper());
                command.Parameters.AddWithValue("@8", txtBoxVeriListesi[10].Trim().ToUpper());
                command.Parameters.AddWithValue("@9", txtBoxVeriListesi[4].Trim().ToUpper());
                command.Parameters.AddWithValue("@10", txtBoxVeriListesi[5].Trim().ToUpper());
                command.Parameters.AddWithValue("@11", txtBoxVeriListesi[8].Trim().ToUpper());

                try
                {
                    bool kayitDurumu = Temel_Metodlar.SqlKomutCalistir(command);
                    if (kayitDurumu)
                    {
                        MessageBox.Show("Kitap kayıt işlemi başarılı.", "Kitap Ekleme", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Kitap kayıt işlemi başarısız.", "Kitap Ekleme Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    command.Dispose();
                    uCkitapDetay1.Temizle();
                }
                catch (Exception)
                {
                    MessageBox.Show("Kitap kayıt işlemi başarısız.", "Kitap Ekleme Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else
            {
                MessageBox.Show("Kitap Adı, Katap Yazarı ve Barkod No alanları boş geçilemez.", "Kitap Ekleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnTemizle_Click(object sender, EventArgs e)
        {
            uCkitapDetay1.Temizle();
        }

        private void UCkitapEkle_Load(object sender, EventArgs e)
        {
            uCkitapDetay1.Temizle();
            uCkitapDetay1.DbVerileriGetir();
        }

    }
}
