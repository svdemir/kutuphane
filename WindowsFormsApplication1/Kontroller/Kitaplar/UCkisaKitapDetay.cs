﻿using System.Linq;
using System.Windows.Forms;

namespace ktProje.Kontroller.Kitaplar
{
    public partial class UCkisaKitapDetay : UserControl
    {
        public UCkisaKitapDetay()
        {
            InitializeComponent();
        }
        
        public void VeriEkle(string kAdi, string kYazari, string kTuru, string kYayinevi, string barkod)
        {
            textBox1.Text = kAdi;
            textBox2.Text = kYazari;
            textBox3.Text = kTuru;
            textBox4.Text = kYayinevi;
            textBox5.Text = barkod;
        }

        public void Temizle()
        {
            foreach (TextBox textbox in groupBox1.Controls.OfType<TextBox>())
            {
                textbox.Text = string.Empty;
            }

        }
    }
}
