﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace ktProje.Kontroller.Kitaplar
{
    public partial class UCkitapDetay : UserControl
    {
        public UCkitapDetay()
        {
            InitializeComponent();
        }

        public List<string> VeriDon()
        {
            List<string> veriListesi = new List<string>();
            veriListesi.Add(textBox1.Text);
            veriListesi.Add(textBox2.Text);
            veriListesi.Add(comboBox1.Text);
            veriListesi.Add(comboBox3.Text);
            veriListesi.Add(numericSayfa.Text);
            veriListesi.Add(numericBaskiYil.Text);
            veriListesi.Add(numericBarkod.Text);
            veriListesi.Add(numericDNo.Text);
            veriListesi.Add(numericISBN.Text);
            veriListesi.Add(comboBox2.Text);
            veriListesi.Add(textBox11.Text);
            return veriListesi;
        }

        public void VeriEkle(string kAdi, string kYazari, string kTuru, string kYayinevi, string sayfa, string baskiYil, string barkod, string dNo, string isbn, string dolap, string raf)
        {
            textBox1.Text = kAdi;
            textBox2.Text = kYazari;
            comboBox1.Text = kTuru;
            comboBox3.Text = kYayinevi;
            numericSayfa.Text = sayfa;
            numericBaskiYil.Text = baskiYil;
            numericBarkod.Text = barkod;
            numericDNo.Text = dNo;
            numericISBN.Text = isbn;
            comboBox2.Text = dolap;
            textBox11.Text = raf;
        }
        
        public void Temizle()
        {
            foreach (TextBox textbox in groupBox1.Controls.OfType<TextBox>())
            {
                textbox.Text = string.Empty;
            }
            foreach (NumericUpDown numeric in groupBox1.Controls.OfType<NumericUpDown>())
            {
                numeric.Text = string.Empty;
            }
            foreach (ComboBox combo in groupBox1.Controls.OfType<ComboBox>())
            {
                combo.Text = string.Empty;
            }
        }

        private void UCkitapDetay_Load(object sender, EventArgs e)
        {
            
        }

        public void DbVerileriGetir()
        {
            try
            {
                string tur = "SELECT * FROM TURLER ORDER BY TUR ASC";
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(tur);
                comboBox1.DataSource = ds.Tables[0];
                comboBox1.DisplayMember = "TUR";
                comboBox1.ValueMember = "TUR";
                comboBox1.SelectedIndex = -1;


                string dolap = "SELECT * FROM DOLAPLAR ORDER BY DOLAP ASC";
                DataSet ds2 = Temel_Metodlar.SqlKomutCalistirSorgu(dolap);
                comboBox2.DataSource = ds2.Tables[0];
                comboBox2.DisplayMember = "DOLAP";
                comboBox2.ValueMember = "DOLAP";
                comboBox2.SelectedIndex = -1;

                string yayinEvi = "SELECT * FROM YAYINEVLERI ORDER BY YAYINEVI ASC";
                DataSet ds3 = Temel_Metodlar.SqlKomutCalistirSorgu(yayinEvi);
                comboBox3.DataSource = ds3.Tables[0];
                comboBox3.DisplayMember = "YAYINEVI";
                comboBox3.SelectedIndex = -1;

                ds.Dispose();
                ds2.Dispose();
                ds3.Dispose();

            }
            catch (Exception)
            {
                MessageBox.Show("Kitap verileri getirme işlemi gerçekleştirilemedi.", "Kitap Detayları", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;               
            }
            
        }


    }
}
