﻿using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data.SQLite;
using System.IO;

namespace ktProje.Kontroller.Kitaplar
{
    public partial class UCtopluKitapEkle : UserControl
    {
        public UCtopluKitapEkle()
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
        }

        string DosyaYolu = "";

        DataSet ds = new DataSet();
        DataTable dt; //hatalı kayıtlar için oluşturulan tablo.

        #region Excel Okuma İşlemleri

        private void btnKitapListesiGetir_Click(object sender, EventArgs e)
        {
            OpenFileDialog file = new OpenFileDialog();
            file.RestoreDirectory = true;
            file.CheckFileExists = false;
            file.Title = "Dosyası Seçiniz..";
            if (file.ShowDialog() == DialogResult.OK)
            {
                DosyaYolu = file.FileName;
                backgroundWorker1.RunWorkerAsync();
            }
        }

        private void VerileriGetir()
        {
            string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + DosyaYolu + "; Extended Properties=Excel 12.0";
            OleDbConnection baglanti = new OleDbConnection(connectionString);

            try
            {
                baglanti.Open();
                using (var adapter = new OleDbDataAdapter("SELECT * FROM [Sayfa1$]", connectionString))
                {
                    adapter.Fill(ds);
                    btnKitapListesiGetir.Enabled = false;
                    btnKaydet.Enabled = true;
                    baglanti.Close();
                    baglanti.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Excel dosyası hatalı."+ ex.Message, "Kitap Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            VerileriGetir();
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (ds.Tables.Count != 0)
            {
                dataGridView1.DataSource = ds.Tables[0];
                dataGridView1.AllowUserToAddRows = false;
                lblKayit.Text = dataGridView1.RowCount.ToString();
                lblKayit.Refresh();
            }
        }


        #endregion

        #region Database Kayıt İşlemleri

        private void VerileriYaz()
        {
            dt = ds.Tables[0].Clone();
            int hataSayisi = 0;

            var dbcon = new SQLiteConnection("Data Source=db.sqlite;Version=3;");
            dbcon.Open();
            SQLiteCommand sqlComm;
            sqlComm = new SQLiteCommand("begin", dbcon);
            sqlComm.ExecuteNonQuery();
            //---INSIDE LOOP
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                DataGridViewRow Row = dataGridView1.Rows[i];                
                string komut = "INSERT INTO KITAPLAR ([ADI], [YAZAR] ,[TUR],[DNO], [BARKOD], [YAYINEVI], [DOLAP],[RAF],[SAYFASAYISI]) VALUES (@1,@2,@3,@4,@5,@6,@7,@8,@9)";
                sqlComm = new SQLiteCommand(komut, dbcon);
                sqlComm.Parameters.AddWithValue("@1", Row.Cells[0].Value.ToString().Trim().ToUpper());
                sqlComm.Parameters.AddWithValue("@2", Row.Cells[1].Value.ToString().Trim().ToUpper());
                sqlComm.Parameters.AddWithValue("@3", Row.Cells[2].Value.ToString().Trim().ToUpper());
                sqlComm.Parameters.AddWithValue("@4", Row.Cells[3].Value.ToString().Trim().ToUpper());
                sqlComm.Parameters.AddWithValue("@5", Row.Cells[4].Value.ToString().Trim().ToUpper());
                sqlComm.Parameters.AddWithValue("@6", Row.Cells[5].Value.ToString().Trim().ToUpper());
                sqlComm.Parameters.AddWithValue("@7", Row.Cells[6].Value.ToString().Trim().ToUpper());
                sqlComm.Parameters.AddWithValue("@8", Row.Cells[7].Value.ToString().Trim().ToUpper());
                sqlComm.Parameters.AddWithValue("@9", Row.Cells[8].Value.ToString().Trim().ToUpper());                

                try
                {
                    sqlComm.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    DataRow rw = dt.NewRow();
                    rw.ItemArray = new object[] { Row.Cells[0].Value, Row.Cells[1].Value, Row.Cells[2].Value, Row.Cells[3].Value, Row.Cells[4].Value, Row.Cells[5].Value, Row.Cells[6].Value, Row.Cells[7].Value };
                    dt.Rows.Add(rw);
                    hataSayisi++;
                    label2.Text = hataSayisi.ToString();
                    label2.Refresh();
                }
            }
            //---END LOOP
            sqlComm = new SQLiteCommand("end", dbcon);
            sqlComm.ExecuteNonQuery();
            dbcon.Close();
            dbcon.Dispose();
            sqlComm.Dispose();
        }



        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                VerileriYaz();
            }
            catch (Exception)
            {
                MessageBox.Show("Genel aktarma işlemi hatası.", "Kitap Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void backgroundWorker2_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void backgroundWorker2_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            dataGridView1.DataSource = dt;
            dataGridView1.Refresh();
            lblBasarili.Text = (Convert.ToInt32(lblKayit.Text) - Convert.ToInt32(label2.Text)).ToString();
            lblBasarili.Refresh();
            MessageBox.Show("Aktarma işlemi tamamlandı.", "Kitap Listesi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            ds.Dispose();
            dt.Dispose();
            this.ParentForm.Enabled = true;

        }

        private void btnKaydet_Click(object sender, EventArgs e)
        {
            this.ParentForm.Enabled = false;
            backgroundWorker2.RunWorkerAsync();
        }

        #endregion

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void btnSablon_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xlsx)|*.xlsx";
            sfd.FileName = "kitaplarSablon.xlsx";

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                var a = ktProje.Properties.Resources.kitaplarSablon;
                FileStream fileStream = new FileStream(sfd.FileName, FileMode.Create);
                fileStream.Write(a, 0, a.Length);
                fileStream.Close();
                fileStream.Dispose();
            }
            MessageBox.Show("Şablon indirme işlemi tamamlandı.", "Kitap Listesi", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
