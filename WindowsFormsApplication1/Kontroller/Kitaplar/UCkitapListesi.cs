﻿using DGVPrinterHelper;
using System;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace ktProje.Kontroller.Kitaplar
{
    public partial class UCkitapListesi : UserControl
    {
        public UCkitapListesi()
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
        }

        private void btnKitapListesiGetir_Click(object sender, EventArgs e)
        {
            string komut = "SELECT * FROM KITAPLAR";
            try
            {
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                dataGridView1.DataSource = ds.Tables[0];
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi. Kitap listesinde hatalar var.", "Kitap Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            dataGridView1.Columns.RemoveAt(11);
            dataGridView1.Columns.RemoveAt(10);
            dataGridView1.Columns.RemoveAt(9);
            dataGridView1.Columns.RemoveAt(0);


            dataGridView1.Columns[0].HeaderText = "Kitap Adı";
            dataGridView1.Columns[1].HeaderText = "Yazarı";
            dataGridView1.Columns[2].HeaderText = "Türü";
            dataGridView1.Columns[3].HeaderText = "Demirbaş No";
            dataGridView1.Columns[4].HeaderText = "Barkod";
            dataGridView1.Columns[5].HeaderText = "Yayınevi";
            dataGridView1.Columns[6].HeaderText = "Dolap";
            dataGridView1.Columns[7].HeaderText = "Raf";

            dataGridView1.Columns[0].Width = 250;
            dataGridView1.Columns[1].Width = 120;
            dataGridView1.Columns[2].Width = 75;
            dataGridView1.Columns[3].Width = 70;
            dataGridView1.Columns[4].Width = 70;
            dataGridView1.Columns[5].Width = 115;
            dataGridView1.Columns[6].Width = 110;
            dataGridView1.Columns[7].Width = 60;

            dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Temel_Metodlar.Numaralandir(dataGridView1);
            dataGridView1.AllowUserToAddRows = false;
            btnKitapListesiGetir.Enabled = false;
            lblKitapSayisi.Text = dataGridView1.Rows.Count.ToString();
            groupBox1.Visible = true;
            btnKitapListesiYazdır.Enabled = true;
        }

        private void btnKitapListesiYazdır_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Yazdırma işleminde ilk başta yazıcıyı seçip, önizleme yaparak sayfa yönlendirme (Yatay önerilir.) , yazdırılacak sayfa sayısı ayarlarını düzenleyip, yazdır butonuna basınız.", "Yazdırma İşlemi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            DGVPrinter printer = new DGVPrinter();
            printer.Title = "Kütüphane Kitap Listesi";
            printer.PageNumbers = true;
            printer.PageNumberInHeader = true;
            printer.ShowTotalPageNumber = true;
            printer.PorportionalColumns = true;
            printer.HeaderCellAlignment = StringAlignment.Near;

            string okulAdi;
            try
            {
                string komut = "SELECT * FROM AYARLAR";
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                okulAdi = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi. Okul adı girilmemiş, Ayarlar Kısmından ekleyebilirsiniz.", "Kitap Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            printer.Footer = okulAdi + " - " + DateTime.Now.ToLongDateString();
            printer.FooterAlignment = StringAlignment.Center;

            dataGridView1.Columns.Remove("DNO");
            dataGridView1.Columns.Remove("YAYINEVI");
            dataGridView1.Columns.Remove("DOLAP");
            dataGridView1.Columns.Remove("RAF");

            printer.PrintPreviewZoom = 0.7;
            printer.PrintPreviewDataGridView(dataGridView1);
        }

        #region Excel Aktarma
        SaveFileDialog sfd;
        private void btnExcelAktar_Click(object sender, EventArgs e)
        {
            this.ParentForm.Enabled = false;

            try
            {
                string komut = "SELECT * FROM KITAPLAR";
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                dataGridView2.DataSource = ds.Tables[0];
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi. Kitap listesinde hatalar var.", "Kitap Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.ParentForm.Enabled = true;
                return;
            }
            ExcelYazdir();
            btnExcelAktar.Enabled = false;
        }

        private void ExcelYazdir()
        {
            sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xlsx)|*.xlsx";
            sfd.FileName = "kitaplarExport.xlsx";

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    var a = ktProje.Properties.Resources.kitaplarSablon;
                    FileStream fileStream = new FileStream(sfd.FileName, FileMode.Create);
                    fileStream.Write(a, 0, a.Length);
                    fileStream.Close();
                    fileStream.Dispose();
                    backgroundWorker1.RunWorkerAsync();
                }
                catch (Exception)
                {
                    MessageBox.Show("İşlem gerçekleştirilemedi. Dosya oluşturulurken hata oluştu tekrar deneyiniz.", "Kitap Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.ParentForm.Enabled = true;
                    return;
                }
            }
            else
            {
            }
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                string connectionString2 = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + sfd.FileName + @"; Extended Properties=""Excel 12.0 Xml;HDR=YES""";
                OleDbConnection baglanti2 = new OleDbConnection(connectionString2);
                baglanti2.Open();

                for (int i = 0; i < dataGridView2.RowCount; i++)
                {
                    DataGridViewRow Row = dataGridView2.Rows[i];

                    OleDbCommand komut = new OleDbCommand("INSERT INTO [Sayfa1$] ([Kitap Adı], [Yazarı] ,  [Türü],[Demirbaş No], [Barkod], [Yayınevi], [Dolap],[Raf],[Sayfa Sayısı]) VALUES ('" + Row.Cells[1].Value + "','" + Row.Cells[2].Value + "','" + Row.Cells[3].Value + "','" + Row.Cells[4].Value + "','" + Row.Cells[5].Value + "','" + Row.Cells[6].Value + "','" + Row.Cells[7].Value + "','" + Row.Cells[8].Value + "','" + Row.Cells[9].Value + "'" + ")", baglanti2);

                    try
                    {
                        komut.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                    }
                }
                baglanti2.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Aktarma işlemi hatası.", "Kitap Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.ParentForm.Enabled = true;
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show("Aktarma işlemi tamamlandı.", "Kitap Listesi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.ParentForm.Enabled = true;

        }

        private void backgroundWorker1_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
        }

        #endregion

        private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Temel_Metodlar.Numaralandir(dataGridView1);
        }


    }
}
