﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SQLite;

namespace ktProje.Kontroller.Kitaplar
{
    public partial class UCtopluKitapDuzenle : UserControl
    {
        public UCtopluKitapDuzenle()
        {
            InitializeComponent();
        }

        SQLiteDataAdapter adapter;
        DataSet ds;

        private void VerileriGetir()
        {
            try
            {
                string komut = "SELECT * FROM KITAPLAR";
                var DBbaglanti = new SQLiteConnection("Data Source=db.sqlite;Version=3;");
                SQLiteCommand command = new SQLiteCommand(komut, DBbaglanti);
                DBbaglanti.Open();
                adapter = new SQLiteDataAdapter(command);
                ds = new DataSet();
                adapter.Fill(ds);
                DBbaglanti.Close();

                SQLiteCommandBuilder cb = new SQLiteCommandBuilder(adapter);
                cb.ConflictOption = ConflictOption.OverwriteChanges;
                dataGridView1.DataSource = ds.Tables[0];

                

                TurleriGetir();

                YayinevleriniGetir();

                DolaplariGetir();

                dataGridView1.Columns.RemoveAt(7);
                dataGridView1.Columns.RemoveAt(6);
                
                dataGridView1.Columns.RemoveAt(3);
                dataGridView1.Columns[0].Visible = false;

                dataGridView1.Columns[0].HeaderText = "ID";
                dataGridView1.Columns[1].HeaderText = "Kitap Adı";
                dataGridView1.Columns[2].HeaderText = "Yazarı";
                dataGridView1.Columns[3].HeaderText = "Demirbaş No";
                dataGridView1.Columns[4].HeaderText = "Barkod";
                dataGridView1.Columns[5].HeaderText = "Raf";
                dataGridView1.Columns[6].HeaderText = "Sayfa Sayısı";
                dataGridView1.Columns[7].HeaderText = "Basım Yılı";
                dataGridView1.Columns[8].HeaderText = "ISBN No";
                dataGridView1.Columns[0].Width = 50;
                dataGridView1.Columns[1].Width = 250;
                dataGridView1.Columns[2].Width = 120;
                dataGridView1.Columns[3].Width = 75;
                dataGridView1.Columns[4].Width = 70;
                dataGridView1.Columns[5].Width = 70;
                dataGridView1.Columns[6].Width = 120;
                dataGridView1.Columns[7].Width = 120;
                dataGridView1.Columns[8].Width = 60;
                dataGridView1.Columns[9].Width = 120;
                dataGridView1.Columns[10].Width = 120;
                dataGridView1.Columns[11].Width = 120;

                dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                Temel_Metodlar.Numaralandir(dataGridView1);
                dataGridView1.AllowUserToAddRows = false;
                btnKitapListesiGetir.Enabled = false;
                lblKitapSayisi.Text = dataGridView1.Rows.Count.ToString();
                groupBox1.Visible = true;
                btnGuncelle.Enabled = true;
                btnSil.Enabled = true;
            }
            catch (Exception)
            {
                MessageBox.Show("Kitap listesi getirilemedi.", "Toplu Kitap Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }           
        }

        private void DolaplariGetir()
        {
            try
            {
                string komutDolap = "SELECT * FROM DOLAPLAR ORDER BY DOLAP ASC";
                DataSet dsDolap = Temel_Metodlar.SqlKomutCalistirSorgu(komutDolap);
                DataGridViewComboBoxColumn columnDolap = new DataGridViewComboBoxColumn();
                columnDolap.DataSource = dsDolap.Tables[0];
                columnDolap.DataPropertyName = "DOLAP";
                columnDolap.DisplayMember = "DOLAP";
                columnDolap.DisplayIndex = 8;
                columnDolap.HeaderText = "Dolap";
                dataGridView1.Columns.Add(columnDolap);
                dsDolap.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("Dolap listesi getirilemedi.", "Toplu Kitap Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void YayinevleriniGetir()
        {
            try
            {
                string komutYayinevi = "SELECT * FROM YAYINEVLERI ORDER BY YAYINEVI ASC";
                DataSet dsYayinevi = Temel_Metodlar.SqlKomutCalistirSorgu(komutYayinevi);
                DataGridViewComboBoxColumn columnYayinevi = new DataGridViewComboBoxColumn();
                columnYayinevi.DataSource = dsYayinevi.Tables[0];
                columnYayinevi.DataPropertyName = "YAYINEVI";
                columnYayinevi.DisplayMember = "YAYINEVI";
                columnYayinevi.DisplayIndex = 7;
                columnYayinevi.HeaderText = "Yayınevi";
                dataGridView1.Columns.Add(columnYayinevi);
                dsYayinevi.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("Yayınevi listesi getirilemedi.", "Toplu Kitap Düzenleme.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void TurleriGetir()
        {
            try
            {
                string komutTur = "SELECT * FROM TURLER ORDER BY TUR ASC";
                DataSet dsTur = Temel_Metodlar.SqlKomutCalistirSorgu(komutTur);
                DataGridViewComboBoxColumn columnTur = new DataGridViewComboBoxColumn();
                columnTur.DataSource = dsTur.Tables[0];
                columnTur.DataPropertyName = "TUR";
                columnTur.DisplayMember = "TUR";
                columnTur.DisplayIndex = 3;
                columnTur.HeaderText = "Türü";
                dataGridView1.Columns.Add(columnTur);
                dsTur.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("Tür listesi getirilemedi.", "Toplu Kitap Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Temel_Metodlar.Numaralandir(dataGridView1);
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void btnKitapListesiGetir_Click(object sender, EventArgs e)
        {
            VerileriGetir();
        }

        private void btnGuncelle_Click(object sender, EventArgs e)
        {
            try
            {
                adapter.Update(ds);
                MessageBox.Show("Güncelleme işlemi başarılı.", "Toplu Kitap Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Information);
                adapter.Dispose();
                ds.Dispose();
            }
            catch (Exception )
            {
                MessageBox.Show("Güncelleme işlemi başarısız.", "Toplu Kitap Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSil_Click(object sender, EventArgs e)
        {
            try
            {
                string silinecekKitapID = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                SQLiteCommand command = new SQLiteCommand("DELETE FROM KITAPLAR WHERE ID=@1");
                command.Parameters.AddWithValue("@1", silinecekKitapID);
                Temel_Metodlar.SqlKomutCalistir(command);
                VerileriGetir();
                MessageBox.Show("Silme işlemi başarılı.", "Toplu Kitap Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Information);
                command.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("Silme işlemi başarısız.", "Toplu Kitap Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
