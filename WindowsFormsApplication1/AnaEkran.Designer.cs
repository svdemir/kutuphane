﻿namespace ktProje
{
    partial class AnaEkran
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AnaEkran));
            this.ribbon1 = new System.Windows.Forms.Ribbon();
            this.ribbonSeparator1 = new System.Windows.Forms.RibbonSeparator();
            this.ribbonSeparator2 = new System.Windows.Forms.RibbonSeparator();
            this.ribbonSeparator3 = new System.Windows.Forms.RibbonSeparator();
            this.ribbonSeparator4 = new System.Windows.Forms.RibbonSeparator();
            this.ribbonSeparator5 = new System.Windows.Forms.RibbonSeparator();
            this.ribbonSeparator6 = new System.Windows.Forms.RibbonSeparator();
            this.ribbonTab1 = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel1 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel2 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel12 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel13 = new System.Windows.Forms.RibbonPanel();
            this.ribbonTab2 = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel3 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel4 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel5 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel15 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel19 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel23 = new System.Windows.Forms.RibbonPanel();
            this.ribbonTab4 = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel6 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel7 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel14 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel16 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel18 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel22 = new System.Windows.Forms.RibbonPanel();
            this.ribbonTab3 = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel8 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel9 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel17 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel24 = new System.Windows.Forms.RibbonPanel();
            this.ribbonTab5 = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel10 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel11 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel20 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel21 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel25 = new System.Windows.Forms.RibbonPanel();
            this.anaPanel = new System.Windows.Forms.Panel();
            this.ribbonOrbMenuItem1 = new System.Windows.Forms.RibbonOrbMenuItem();
            this.ribbonOrbMenuItem2 = new System.Windows.Forms.RibbonOrbMenuItem();
            this.ribbonOrbMenuItem3 = new System.Windows.Forms.RibbonOrbMenuItem();
            this.ribbonOrbMenuItem4 = new System.Windows.Forms.RibbonOrbMenuItem();
            this.ribbonOrbMenuItem5 = new System.Windows.Forms.RibbonOrbMenuItem();
            this.ribbonOrbMenuItem6 = new System.Windows.Forms.RibbonOrbMenuItem();
            this.ribbonOrbMenuItem7 = new System.Windows.Forms.RibbonOrbMenuItem();
            this.btnOduncVer = new System.Windows.Forms.RibbonButton();
            this.btnTeslimAl = new System.Windows.Forms.RibbonButton();
            this.btnUyedekiKitaplar = new System.Windows.Forms.RibbonButton();
            this.btnSuresiGecen = new System.Windows.Forms.RibbonButton();
            this.btnKitapListesi = new System.Windows.Forms.RibbonButton();
            this.btnKitapEkle = new System.Windows.Forms.RibbonButton();
            this.btnKitapDüzenle = new System.Windows.Forms.RibbonButton();
            this.btnKitapSil = new System.Windows.Forms.RibbonButton();
            this.ribbonButton1 = new System.Windows.Forms.RibbonButton();
            this.btnExcelKitapEkle = new System.Windows.Forms.RibbonButton();
            this.btnUyeListesi = new System.Windows.Forms.RibbonButton();
            this.btnUyeEkle = new System.Windows.Forms.RibbonButton();
            this.btnUyeDuzenle = new System.Windows.Forms.RibbonButton();
            this.btnUyeSil = new System.Windows.Forms.RibbonButton();
            this.btnUyeTopluDuzenle = new System.Windows.Forms.RibbonButton();
            this.btnTopluUye = new System.Windows.Forms.RibbonButton();
            this.btnKitapOkuyanlar = new System.Windows.Forms.RibbonButton();
            this.btnOkunanKitaplar = new System.Windows.Forms.RibbonButton();
            this.btnOgrOkuduguKitaplar = new System.Windows.Forms.RibbonButton();
            this.btnStatistikDuzenle = new System.Windows.Forms.RibbonButton();
            this.btnGenelAyarlar = new System.Windows.Forms.RibbonButton();
            this.btnBarcode = new System.Windows.Forms.RibbonButton();
            this.ribbonButton2 = new System.Windows.Forms.RibbonButton();
            this.btnYayineviDuz = new System.Windows.Forms.RibbonButton();
            this.btnAppGuncelle = new System.Windows.Forms.RibbonButton();
            this.ribbonButton3 = new System.Windows.Forms.RibbonButton();
            this.ribbonButton4 = new System.Windows.Forms.RibbonButton();
            this.SuspendLayout();
            // 
            // ribbon1
            // 
            this.ribbon1.CaptionBarVisible = false;
            this.ribbon1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.ribbon1.Location = new System.Drawing.Point(0, 0);
            this.ribbon1.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.ribbon1.Minimized = false;
            this.ribbon1.Name = "ribbon1";
            // 
            // 
            // 
            this.ribbon1.OrbDropDown.BorderRoundness = 8;
            this.ribbon1.OrbDropDown.CausesValidation = false;
            this.ribbon1.OrbDropDown.ContentRecentItemsMinWidth = 0;
            this.ribbon1.OrbDropDown.Font = new System.Drawing.Font("Segoe UI", 25F, System.Drawing.FontStyle.Bold);
            this.ribbon1.OrbDropDown.Location = new System.Drawing.Point(0, 0);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.ribbonOrbMenuItem1);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.ribbonSeparator1);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.ribbonOrbMenuItem2);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.ribbonSeparator2);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.ribbonOrbMenuItem3);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.ribbonSeparator3);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.ribbonOrbMenuItem4);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.ribbonSeparator4);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.ribbonOrbMenuItem5);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.ribbonSeparator5);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.ribbonOrbMenuItem6);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.ribbonSeparator6);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.ribbonOrbMenuItem7);
            this.ribbon1.OrbDropDown.Name = "";
            this.ribbon1.OrbDropDown.OptionItemsPadding = 0;
            this.ribbon1.OrbDropDown.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ribbon1.OrbDropDown.Size = new System.Drawing.Size(162, 398);
            this.ribbon1.OrbDropDown.TabIndex = 0;
            this.ribbon1.OrbImage = null;
            this.ribbon1.OrbStyle = System.Windows.Forms.RibbonOrbStyle.Office_2010;
            this.ribbon1.OrbText = "Menü";
            // 
            // 
            // 
            this.ribbon1.QuickAcessToolbar.Visible = false;
            this.ribbon1.RibbonTabFont = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Bold);
            this.ribbon1.Size = new System.Drawing.Size(1042, 138);
            this.ribbon1.TabIndex = 0;
            this.ribbon1.Tabs.Add(this.ribbonTab1);
            this.ribbon1.Tabs.Add(this.ribbonTab2);
            this.ribbon1.Tabs.Add(this.ribbonTab4);
            this.ribbon1.Tabs.Add(this.ribbonTab3);
            this.ribbon1.Tabs.Add(this.ribbonTab5);
            this.ribbon1.TabsMargin = new System.Windows.Forms.Padding(12, 2, 20, 0);
            this.ribbon1.Text = "ribbon1";
            this.ribbon1.ThemeColor = System.Windows.Forms.RibbonTheme.Blue;
            this.ribbon1.ActiveTabChanged += new System.EventHandler(this.ribbon1_ActiveTabChanged);
            // 
            // ribbonTab1
            // 
            this.ribbonTab1.Panels.Add(this.ribbonPanel1);
            this.ribbonTab1.Panels.Add(this.ribbonPanel2);
            this.ribbonTab1.Panels.Add(this.ribbonPanel12);
            this.ribbonTab1.Panels.Add(this.ribbonPanel13);
            this.ribbonTab1.Text = "ÖDÜNÇ";
            // 
            // ribbonPanel1
            // 
            this.ribbonPanel1.ButtonMoreVisible = false;
            this.ribbonPanel1.Items.Add(this.btnOduncVer);
            this.ribbonPanel1.Text = "Ödünç Ver";
            // 
            // ribbonPanel2
            // 
            this.ribbonPanel2.ButtonMoreVisible = false;
            this.ribbonPanel2.Items.Add(this.btnTeslimAl);
            this.ribbonPanel2.Text = "Teslim Al";
            // 
            // ribbonPanel12
            // 
            this.ribbonPanel12.ButtonMoreVisible = false;
            this.ribbonPanel12.Items.Add(this.btnUyedekiKitaplar);
            this.ribbonPanel12.Text = "Üyelerdeki Kitaplar";
            // 
            // ribbonPanel13
            // 
            this.ribbonPanel13.ButtonMoreVisible = false;
            this.ribbonPanel13.Items.Add(this.btnSuresiGecen);
            this.ribbonPanel13.Text = "Süresi Geçenler";
            // 
            // ribbonTab2
            // 
            this.ribbonTab2.Panels.Add(this.ribbonPanel3);
            this.ribbonTab2.Panels.Add(this.ribbonPanel4);
            this.ribbonTab2.Panels.Add(this.ribbonPanel5);
            this.ribbonTab2.Panels.Add(this.ribbonPanel15);
            this.ribbonTab2.Panels.Add(this.ribbonPanel19);
            this.ribbonTab2.Panels.Add(this.ribbonPanel23);
            this.ribbonTab2.Text = "KİTAPLAR";
            // 
            // ribbonPanel3
            // 
            this.ribbonPanel3.ButtonMoreVisible = false;
            this.ribbonPanel3.Items.Add(this.btnKitapListesi);
            this.ribbonPanel3.Text = "Kitap Listesi";
            // 
            // ribbonPanel4
            // 
            this.ribbonPanel4.ButtonMoreVisible = false;
            this.ribbonPanel4.Items.Add(this.btnKitapEkle);
            this.ribbonPanel4.Text = "Kitap Ekle";
            // 
            // ribbonPanel5
            // 
            this.ribbonPanel5.ButtonMoreVisible = false;
            this.ribbonPanel5.Items.Add(this.btnKitapDüzenle);
            this.ribbonPanel5.Text = "Kitap Düzenle";
            // 
            // ribbonPanel15
            // 
            this.ribbonPanel15.ButtonMoreVisible = false;
            this.ribbonPanel15.Items.Add(this.btnKitapSil);
            this.ribbonPanel15.Text = "Kitap Sil";
            // 
            // ribbonPanel19
            // 
            this.ribbonPanel19.ButtonMoreVisible = false;
            this.ribbonPanel19.Items.Add(this.ribbonButton1);
            this.ribbonPanel19.Text = "Toplu Kitap Düzenle";
            // 
            // ribbonPanel23
            // 
            this.ribbonPanel23.ButtonMoreVisible = false;
            this.ribbonPanel23.Items.Add(this.btnExcelKitapEkle);
            this.ribbonPanel23.Text = "Excel Kitap Ekle";
            // 
            // ribbonTab4
            // 
            this.ribbonTab4.Panels.Add(this.ribbonPanel6);
            this.ribbonTab4.Panels.Add(this.ribbonPanel7);
            this.ribbonTab4.Panels.Add(this.ribbonPanel14);
            this.ribbonTab4.Panels.Add(this.ribbonPanel16);
            this.ribbonTab4.Panels.Add(this.ribbonPanel18);
            this.ribbonTab4.Panels.Add(this.ribbonPanel22);
            this.ribbonTab4.Text = "ÜYELER";
            // 
            // ribbonPanel6
            // 
            this.ribbonPanel6.ButtonMoreVisible = false;
            this.ribbonPanel6.Items.Add(this.btnUyeListesi);
            this.ribbonPanel6.Text = "Üye Listesi";
            // 
            // ribbonPanel7
            // 
            this.ribbonPanel7.ButtonMoreVisible = false;
            this.ribbonPanel7.Items.Add(this.btnUyeEkle);
            this.ribbonPanel7.Text = "Üye Ekle";
            // 
            // ribbonPanel14
            // 
            this.ribbonPanel14.ButtonMoreVisible = false;
            this.ribbonPanel14.Items.Add(this.btnUyeDuzenle);
            this.ribbonPanel14.Text = "Üye Düzenle";
            // 
            // ribbonPanel16
            // 
            this.ribbonPanel16.ButtonMoreVisible = false;
            this.ribbonPanel16.Items.Add(this.btnUyeSil);
            this.ribbonPanel16.Text = "Üye Sil";
            // 
            // ribbonPanel18
            // 
            this.ribbonPanel18.ButtonMoreVisible = false;
            this.ribbonPanel18.Items.Add(this.btnUyeTopluDuzenle);
            this.ribbonPanel18.Text = "Üye Toplu Düzenle";
            // 
            // ribbonPanel22
            // 
            this.ribbonPanel22.Items.Add(this.btnTopluUye);
            this.ribbonPanel22.Text = "Excel Üye Ekle";
            // 
            // ribbonTab3
            // 
            this.ribbonTab3.Panels.Add(this.ribbonPanel8);
            this.ribbonTab3.Panels.Add(this.ribbonPanel9);
            this.ribbonTab3.Panels.Add(this.ribbonPanel17);
            this.ribbonTab3.Panels.Add(this.ribbonPanel24);
            this.ribbonTab3.Text = "İSTATİSTİKLER";
            // 
            // ribbonPanel8
            // 
            this.ribbonPanel8.ButtonMoreVisible = false;
            this.ribbonPanel8.Items.Add(this.btnKitapOkuyanlar);
            this.ribbonPanel8.Text = "Kitap Okuyanlar";
            // 
            // ribbonPanel9
            // 
            this.ribbonPanel9.ButtonMoreVisible = false;
            this.ribbonPanel9.Items.Add(this.btnOkunanKitaplar);
            this.ribbonPanel9.Text = "Okunan Kitaplar";
            // 
            // ribbonPanel17
            // 
            this.ribbonPanel17.ButtonMoreVisible = false;
            this.ribbonPanel17.Items.Add(this.btnOgrOkuduguKitaplar);
            this.ribbonPanel17.Text = "Öğrencinin Okuduğu Kitaplar";
            // 
            // ribbonPanel24
            // 
            this.ribbonPanel24.ButtonMoreEnabled = false;
            this.ribbonPanel24.ButtonMoreVisible = false;
            this.ribbonPanel24.Items.Add(this.btnStatistikDuzenle);
            this.ribbonPanel24.Text = "İstatistik Düzenle";
            // 
            // ribbonTab5
            // 
            this.ribbonTab5.Panels.Add(this.ribbonPanel10);
            this.ribbonTab5.Panels.Add(this.ribbonPanel11);
            this.ribbonTab5.Panels.Add(this.ribbonPanel20);
            this.ribbonTab5.Panels.Add(this.ribbonPanel21);
            this.ribbonTab5.Panels.Add(this.ribbonPanel25);
            this.ribbonTab5.Text = "AYARLAR";
            // 
            // ribbonPanel10
            // 
            this.ribbonPanel10.ButtonMoreVisible = false;
            this.ribbonPanel10.Items.Add(this.btnGenelAyarlar);
            this.ribbonPanel10.Text = "Genel Ayarlar";
            // 
            // ribbonPanel11
            // 
            this.ribbonPanel11.ButtonMoreVisible = false;
            this.ribbonPanel11.Items.Add(this.btnBarcode);
            this.ribbonPanel11.Text = "Barkod Yazdırma";
            // 
            // ribbonPanel20
            // 
            this.ribbonPanel20.ButtonMoreVisible = false;
            this.ribbonPanel20.Items.Add(this.ribbonButton2);
            this.ribbonPanel20.Text = "Sınıf Düzenleme";
            // 
            // ribbonPanel21
            // 
            this.ribbonPanel21.ButtonMoreVisible = false;
            this.ribbonPanel21.Items.Add(this.btnYayineviDuz);
            this.ribbonPanel21.Text = "Yayınevi Düzenle";
            // 
            // ribbonPanel25
            // 
            this.ribbonPanel25.ButtonMoreVisible = false;
            this.ribbonPanel25.Items.Add(this.btnAppGuncelle);
            this.ribbonPanel25.Text = "Güncelleme";
            // 
            // anaPanel
            // 
            this.anaPanel.BackColor = System.Drawing.Color.Silver;
            this.anaPanel.BackgroundImage = global::ktProje.Properties.Resources.OAYXJ60;
            this.anaPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.anaPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.anaPanel.Location = new System.Drawing.Point(0, 138);
            this.anaPanel.Name = "anaPanel";
            this.anaPanel.Size = new System.Drawing.Size(1042, 542);
            this.anaPanel.TabIndex = 1;
            // 
            // ribbonOrbMenuItem1
            // 
            this.ribbonOrbMenuItem1.DrawIconsBar = false;
            this.ribbonOrbMenuItem1.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.ribbonOrbMenuItem1.Image = global::ktProje.Properties.Resources.house_with_chimney;
            this.ribbonOrbMenuItem1.SmallImage = global::ktProje.Properties.Resources.house_with_chimney;
            this.ribbonOrbMenuItem1.Text = "Ana Ekran";
            this.ribbonOrbMenuItem1.Click += new System.EventHandler(this.ribbonOrbMenuItem1_Click);
            // 
            // ribbonOrbMenuItem2
            // 
            this.ribbonOrbMenuItem2.DrawIconsBar = false;
            this.ribbonOrbMenuItem2.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.ribbonOrbMenuItem2.Image = global::ktProje.Properties.Resources.kbag_of_books__1_;
            this.ribbonOrbMenuItem2.SmallImage = global::ktProje.Properties.Resources.kbag_of_books__1_;
            this.ribbonOrbMenuItem2.Text = "Ödünç";
            this.ribbonOrbMenuItem2.Click += new System.EventHandler(this.ribbonOrbMenuItem2_Click);
            // 
            // ribbonOrbMenuItem3
            // 
            this.ribbonOrbMenuItem3.DrawIconsBar = false;
            this.ribbonOrbMenuItem3.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.ribbonOrbMenuItem3.Image = global::ktProje.Properties.Resources.kLibrary_2_icon;
            this.ribbonOrbMenuItem3.SmallImage = global::ktProje.Properties.Resources.kLibrary_2_icon;
            this.ribbonOrbMenuItem3.Text = "Kitaplar";
            this.ribbonOrbMenuItem3.Click += new System.EventHandler(this.ribbonOrbMenuItem3_Click);
            // 
            // ribbonOrbMenuItem4
            // 
            this.ribbonOrbMenuItem4.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.ribbonOrbMenuItem4.Image = global::ktProje.Properties.Resources.kmeeting;
            this.ribbonOrbMenuItem4.SmallImage = global::ktProje.Properties.Resources.kmeeting;
            this.ribbonOrbMenuItem4.Text = "Üyeler";
            this.ribbonOrbMenuItem4.Click += new System.EventHandler(this.ribbonOrbMenuItem4_Click);
            // 
            // ribbonOrbMenuItem5
            // 
            this.ribbonOrbMenuItem5.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.ribbonOrbMenuItem5.Image = global::ktProje.Properties.Resources.bar_chart;
            this.ribbonOrbMenuItem5.SmallImage = global::ktProje.Properties.Resources.bar_chart;
            this.ribbonOrbMenuItem5.Text = "İstatistikler";
            this.ribbonOrbMenuItem5.Click += new System.EventHandler(this.ribbonOrbMenuItem5_Click);
            // 
            // ribbonOrbMenuItem6
            // 
            this.ribbonOrbMenuItem6.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.ribbonOrbMenuItem6.Image = global::ktProje.Properties.Resources.k1489885830_Settings_5;
            this.ribbonOrbMenuItem6.SmallImage = global::ktProje.Properties.Resources.k1489885830_Settings_5;
            this.ribbonOrbMenuItem6.Text = "Ayarlar";
            this.ribbonOrbMenuItem6.Click += new System.EventHandler(this.ribbonOrbMenuItem6_Click);
            // 
            // ribbonOrbMenuItem7
            // 
            this.ribbonOrbMenuItem7.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.ribbonOrbMenuItem7.Image = global::ktProje.Properties.Resources.error;
            this.ribbonOrbMenuItem7.SmallImage = global::ktProje.Properties.Resources.error;
            this.ribbonOrbMenuItem7.Text = "Çıkış";
            this.ribbonOrbMenuItem7.Click += new System.EventHandler(this.ribbonOrbMenuItem7_Click);
            // 
            // btnOduncVer
            // 
            this.btnOduncVer.Image = global::ktProje.Properties.Resources.bag_of_books__1_;
            this.btnOduncVer.MinimumSize = new System.Drawing.Size(100, 81);
            this.btnOduncVer.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnOduncVer.SmallImage")));
            this.btnOduncVer.Text = "";
            this.btnOduncVer.Click += new System.EventHandler(this.btnOduncVer_Click);
            // 
            // btnTeslimAl
            // 
            this.btnTeslimAl.Image = global::ktProje.Properties.Resources.table_grid_completely_selected_with_crosses;
            this.btnTeslimAl.MinimumSize = new System.Drawing.Size(100, 81);
            this.btnTeslimAl.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnTeslimAl.SmallImage")));
            this.btnTeslimAl.Text = "";
            this.btnTeslimAl.Click += new System.EventHandler(this.btnTeslimAl_Click);
            // 
            // btnUyedekiKitaplar
            // 
            this.btnUyedekiKitaplar.Image = global::ktProje.Properties.Resources.two_students_reading_books_with_right_arrows;
            this.btnUyedekiKitaplar.MinimumSize = new System.Drawing.Size(110, 81);
            this.btnUyedekiKitaplar.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnUyedekiKitaplar.SmallImage")));
            this.btnUyedekiKitaplar.Text = "";
            this.btnUyedekiKitaplar.Click += new System.EventHandler(this.btnUyedekiKitaplar_Click);
            // 
            // btnSuresiGecen
            // 
            this.btnSuresiGecen.DropDownArrowSize = new System.Drawing.Size(0, 0);
            this.btnSuresiGecen.Image = global::ktProje.Properties.Resources.books_and_clock_with_alarm;
            this.btnSuresiGecen.MinimumSize = new System.Drawing.Size(100, 81);
            this.btnSuresiGecen.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnSuresiGecen.SmallImage")));
            this.btnSuresiGecen.Text = "";
            this.btnSuresiGecen.Click += new System.EventHandler(this.btnSuresiGecen_Click);
            // 
            // btnKitapListesi
            // 
            this.btnKitapListesi.Image = global::ktProje.Properties.Resources.Library_2_icon;
            this.btnKitapListesi.MinimumSize = new System.Drawing.Size(89, 81);
            this.btnKitapListesi.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnKitapListesi.SmallImage")));
            this.btnKitapListesi.Text = "";
            this.btnKitapListesi.Click += new System.EventHandler(this.btnKitapListesi_Click);
            // 
            // btnKitapEkle
            // 
            this.btnKitapEkle.Image = global::ktProje.Properties.Resources.Actions_address_book_new_icon;
            this.btnKitapEkle.MinimumSize = new System.Drawing.Size(89, 81);
            this.btnKitapEkle.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnKitapEkle.SmallImage")));
            this.btnKitapEkle.Text = "";
            this.btnKitapEkle.Click += new System.EventHandler(this.btnKitapEkle_Click);
            // 
            // btnKitapDüzenle
            // 
            this.btnKitapDüzenle.Image = global::ktProje.Properties.Resources._1489879888_note_1;
            this.btnKitapDüzenle.MinimumSize = new System.Drawing.Size(89, 81);
            this.btnKitapDüzenle.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnKitapDüzenle.SmallImage")));
            this.btnKitapDüzenle.Text = "";
            this.btnKitapDüzenle.Click += new System.EventHandler(this.btnKitapDüzenle_Click);
            // 
            // btnKitapSil
            // 
            this.btnKitapSil.Image = global::ktProje.Properties.Resources.delete;
            this.btnKitapSil.MinimumSize = new System.Drawing.Size(89, 81);
            this.btnKitapSil.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnKitapSil.SmallImage")));
            this.btnKitapSil.Text = "";
            this.btnKitapSil.Click += new System.EventHandler(this.btnKitapSil_Click);
            // 
            // ribbonButton1
            // 
            this.ribbonButton1.Image = global::ktProje.Properties.Resources.wireframe;
            this.ribbonButton1.MinimumSize = new System.Drawing.Size(123, 84);
            this.ribbonButton1.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton1.SmallImage")));
            this.ribbonButton1.Text = "";
            this.ribbonButton1.Click += new System.EventHandler(this.ribbonButton1_Click);
            // 
            // btnExcelKitapEkle
            // 
            this.btnExcelKitapEkle.Image = global::ktProje.Properties.Resources.notebook;
            this.btnExcelKitapEkle.MinimumSize = new System.Drawing.Size(100, 84);
            this.btnExcelKitapEkle.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnExcelKitapEkle.SmallImage")));
            this.btnExcelKitapEkle.Text = "";
            this.btnExcelKitapEkle.Click += new System.EventHandler(this.btnExcelKitapEkle_Click);
            // 
            // btnUyeListesi
            // 
            this.btnUyeListesi.Image = global::ktProje.Properties.Resources.meeting;
            this.btnUyeListesi.MinimumSize = new System.Drawing.Size(89, 81);
            this.btnUyeListesi.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnUyeListesi.SmallImage")));
            this.btnUyeListesi.Text = "";
            this.btnUyeListesi.Click += new System.EventHandler(this.btnUyeListesi_Click);
            // 
            // btnUyeEkle
            // 
            this.btnUyeEkle.Image = global::ktProje.Properties.Resources._1489882769_user_add;
            this.btnUyeEkle.MinimumSize = new System.Drawing.Size(89, 81);
            this.btnUyeEkle.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnUyeEkle.SmallImage")));
            this.btnUyeEkle.Text = "";
            this.btnUyeEkle.Click += new System.EventHandler(this.btnUyeEkle_Click);
            // 
            // btnUyeDuzenle
            // 
            this.btnUyeDuzenle.Image = global::ktProje.Properties.Resources.Edit_Users_icon;
            this.btnUyeDuzenle.MinimumSize = new System.Drawing.Size(93, 81);
            this.btnUyeDuzenle.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnUyeDuzenle.SmallImage")));
            this.btnUyeDuzenle.Text = "";
            this.btnUyeDuzenle.Click += new System.EventHandler(this.btnUyeDuzenle_Click);
            // 
            // btnUyeSil
            // 
            this.btnUyeSil.Image = global::ktProje.Properties.Resources._1489883600_Block_delete_person_remove_user;
            this.btnUyeSil.MinimumSize = new System.Drawing.Size(95, 81);
            this.btnUyeSil.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnUyeSil.SmallImage")));
            this.btnUyeSil.Text = "";
            this.btnUyeSil.Click += new System.EventHandler(this.btnUyeSil_Click);
            // 
            // btnUyeTopluDuzenle
            // 
            this.btnUyeTopluDuzenle.Image = global::ktProje.Properties.Resources.edit_group_button;
            this.btnUyeTopluDuzenle.MinimumSize = new System.Drawing.Size(111, 81);
            this.btnUyeTopluDuzenle.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnUyeTopluDuzenle.SmallImage")));
            this.btnUyeTopluDuzenle.Text = "";
            this.btnUyeTopluDuzenle.Click += new System.EventHandler(this.btnUyeTopluDuzenle_Click);
            // 
            // btnTopluUye
            // 
            this.btnTopluUye.Image = global::ktProje.Properties.Resources.hierarchical_structure;
            this.btnTopluUye.MinimumSize = new System.Drawing.Size(105, 81);
            this.btnTopluUye.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnTopluUye.SmallImage")));
            this.btnTopluUye.Text = "";
            this.btnTopluUye.Click += new System.EventHandler(this.btnTopluUye_Click);
            // 
            // btnKitapOkuyanlar
            // 
            this.btnKitapOkuyanlar.Image = global::ktProje.Properties.Resources.popularity;
            this.btnKitapOkuyanlar.MaximumSize = new System.Drawing.Size(100, 0);
            this.btnKitapOkuyanlar.MinimumSize = new System.Drawing.Size(104, 81);
            this.btnKitapOkuyanlar.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnKitapOkuyanlar.SmallImage")));
            this.btnKitapOkuyanlar.Text = "";
            this.btnKitapOkuyanlar.ToolTip = "En Çok Kitap Okuyan Öğrencileri Listeler";
            this.btnKitapOkuyanlar.Click += new System.EventHandler(this.btnKitapOkuyanlar_Click);
            // 
            // btnOkunanKitaplar
            // 
            this.btnOkunanKitaplar.Image = global::ktProje.Properties.Resources.analytics;
            this.btnOkunanKitaplar.MinimumSize = new System.Drawing.Size(104, 81);
            this.btnOkunanKitaplar.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnOkunanKitaplar.SmallImage")));
            this.btnOkunanKitaplar.Text = "";
            this.btnOkunanKitaplar.ToolTip = "En Çok Okunan Kitapları Listeler";
            this.btnOkunanKitaplar.Click += new System.EventHandler(this.btnOkunanKitaplar_Click);
            // 
            // btnOgrOkuduguKitaplar
            // 
            this.btnOgrOkuduguKitaplar.Image = global::ktProje.Properties.Resources.group_and_arrow;
            this.btnOgrOkuduguKitaplar.MinimumSize = new System.Drawing.Size(164, 81);
            this.btnOgrOkuduguKitaplar.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnOgrOkuduguKitaplar.SmallImage")));
            this.btnOgrOkuduguKitaplar.Text = "";
            this.btnOgrOkuduguKitaplar.ToolTip = "Öğrencinin Okuduğu Kitaplar";
            this.btnOgrOkuduguKitaplar.Click += new System.EventHandler(this.btnOgrOkuduguKitaplar_Click);
            // 
            // btnStatistikDuzenle
            // 
            this.btnStatistikDuzenle.Image = global::ktProje.Properties.Resources.analizEdit3;
            this.btnStatistikDuzenle.MinimumSize = new System.Drawing.Size(98, 81);
            this.btnStatistikDuzenle.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnStatistikDuzenle.SmallImage")));
            this.btnStatistikDuzenle.Text = "";
            this.btnStatistikDuzenle.ToolTip = "İstatistik Düzenle";
            this.btnStatistikDuzenle.Click += new System.EventHandler(this.btnStatistikDuzenle_Click);
            // 
            // btnGenelAyarlar
            // 
            this.btnGenelAyarlar.Image = global::ktProje.Properties.Resources._1489885830_Settings_5;
            this.btnGenelAyarlar.MinimumSize = new System.Drawing.Size(95, 81);
            this.btnGenelAyarlar.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnGenelAyarlar.SmallImage")));
            this.btnGenelAyarlar.Text = "";
            this.btnGenelAyarlar.Click += new System.EventHandler(this.btnGenelAyarlar_Click);
            // 
            // btnBarcode
            // 
            this.btnBarcode.Image = global::ktProje.Properties.Resources._1489885944_finance_10;
            this.btnBarcode.MinimumSize = new System.Drawing.Size(114, 81);
            this.btnBarcode.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnBarcode.SmallImage")));
            this.btnBarcode.Text = "";
            this.btnBarcode.Click += new System.EventHandler(this.btnBarcode_Click);
            // 
            // ribbonButton2
            // 
            this.ribbonButton2.Image = global::ktProje.Properties.Resources.lesson;
            this.ribbonButton2.MinimumSize = new System.Drawing.Size(100, 81);
            this.ribbonButton2.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton2.SmallImage")));
            this.ribbonButton2.Text = "";
            this.ribbonButton2.Click += new System.EventHandler(this.ribbonButton2_Click_1);
            // 
            // btnYayineviDuz
            // 
            this.btnYayineviDuz.Image = global::ktProje.Properties.Resources.shelf_full;
            this.btnYayineviDuz.MinimumSize = new System.Drawing.Size(105, 81);
            this.btnYayineviDuz.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnYayineviDuz.SmallImage")));
            this.btnYayineviDuz.Text = "";
            this.btnYayineviDuz.Click += new System.EventHandler(this.btnYayineviDuz_Click);
            // 
            // btnAppGuncelle
            // 
            this.btnAppGuncelle.Image = global::ktProje.Properties.Resources.AppUpdate;
            this.btnAppGuncelle.MinimumSize = new System.Drawing.Size(100, 81);
            this.btnAppGuncelle.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnAppGuncelle.SmallImage")));
            this.btnAppGuncelle.Text = "";
            this.btnAppGuncelle.Click += new System.EventHandler(this.btnAppGuncelle_Click);
            // 
            // ribbonButton3
            // 
            this.ribbonButton3.Image = ((System.Drawing.Image)(resources.GetObject("ribbonButton3.Image")));
            this.ribbonButton3.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton3.SmallImage")));
            // 
            // ribbonButton4
            // 
            this.ribbonButton4.Image = ((System.Drawing.Image)(resources.GetObject("ribbonButton4.Image")));
            this.ribbonButton4.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton4.SmallImage")));
            // 
            // AnaEkran
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1042, 680);
            this.Controls.Add(this.anaPanel);
            this.Controls.Add(this.ribbon1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.Name = "AnaEkran";
            this.Text = "Kütüphane Kayıt İşlemleri";
            this.Load += new System.EventHandler(this.AnaEkran_Load);
            this.SizeChanged += new System.EventHandler(this.AnaEkran_SizeChanged);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Ribbon ribbon1;
        private System.Windows.Forms.RibbonTab ribbonTab1;
        private System.Windows.Forms.RibbonPanel ribbonPanel1;
        private System.Windows.Forms.RibbonPanel ribbonPanel2;
        private System.Windows.Forms.RibbonTab ribbonTab2;
        private System.Windows.Forms.RibbonPanel ribbonPanel3;
        private System.Windows.Forms.RibbonPanel ribbonPanel4;
        private System.Windows.Forms.RibbonPanel ribbonPanel5;
        private System.Windows.Forms.RibbonPanel ribbonPanel12;
        private System.Windows.Forms.RibbonPanel ribbonPanel13;
        private System.Windows.Forms.RibbonTab ribbonTab4;
        private System.Windows.Forms.RibbonPanel ribbonPanel6;
        private System.Windows.Forms.RibbonPanel ribbonPanel7;
        private System.Windows.Forms.RibbonTab ribbonTab3;
        private System.Windows.Forms.RibbonPanel ribbonPanel8;
        private System.Windows.Forms.RibbonPanel ribbonPanel9;
        private System.Windows.Forms.RibbonTab ribbonTab5;
        private System.Windows.Forms.RibbonPanel ribbonPanel10;
        private System.Windows.Forms.RibbonPanel ribbonPanel11;
        private System.Windows.Forms.RibbonPanel ribbonPanel15;
        private System.Windows.Forms.RibbonPanel ribbonPanel14;
        private System.Windows.Forms.RibbonPanel ribbonPanel18;
        private System.Windows.Forms.RibbonPanel ribbonPanel16;
        private System.Windows.Forms.RibbonPanel ribbonPanel17;
        private System.Windows.Forms.RibbonButton ribbonButton3;
        private System.Windows.Forms.RibbonButton ribbonButton4;
        private System.Windows.Forms.RibbonButton btnTeslimAl;
        private System.Windows.Forms.RibbonButton btnKitapDüzenle;
        private System.Windows.Forms.RibbonButton btnKitapListesi;
        private System.Windows.Forms.RibbonButton btnKitapEkle;
        private System.Windows.Forms.RibbonButton btnKitapSil;
        private System.Windows.Forms.RibbonButton btnUyeListesi;
        private System.Windows.Forms.RibbonButton btnUyeEkle;
        private System.Windows.Forms.RibbonButton btnUyeDuzenle;
        private System.Windows.Forms.RibbonButton btnUyeTopluDuzenle;
        private System.Windows.Forms.RibbonButton btnUyeSil;
        private System.Windows.Forms.RibbonButton btnKitapOkuyanlar;
        private System.Windows.Forms.RibbonButton btnOkunanKitaplar;
        private System.Windows.Forms.RibbonButton btnOgrOkuduguKitaplar;
        private System.Windows.Forms.RibbonButton btnGenelAyarlar;
        private System.Windows.Forms.RibbonButton btnBarcode;
        private System.Windows.Forms.RibbonButton btnOduncVer;
        private System.Windows.Forms.RibbonButton btnUyedekiKitaplar;
        private System.Windows.Forms.RibbonButton btnSuresiGecen;
        private System.Windows.Forms.RibbonOrbMenuItem ribbonOrbMenuItem1;
        private System.Windows.Forms.RibbonOrbMenuItem ribbonOrbMenuItem2;
        private System.Windows.Forms.RibbonOrbMenuItem ribbonOrbMenuItem3;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator1;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator2;
        private System.Windows.Forms.RibbonOrbMenuItem ribbonOrbMenuItem4;
        private System.Windows.Forms.RibbonOrbMenuItem ribbonOrbMenuItem5;
        private System.Windows.Forms.RibbonOrbMenuItem ribbonOrbMenuItem6;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator3;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator4;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator5;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator6;
        private System.Windows.Forms.RibbonOrbMenuItem ribbonOrbMenuItem7;
        public System.Windows.Forms.Panel anaPanel;
        private System.Windows.Forms.RibbonPanel ribbonPanel19;
        private System.Windows.Forms.RibbonButton ribbonButton1;
        private System.Windows.Forms.RibbonPanel ribbonPanel20;
        private System.Windows.Forms.RibbonButton ribbonButton2;
        private System.Windows.Forms.RibbonPanel ribbonPanel21;
        private System.Windows.Forms.RibbonButton btnYayineviDuz;
        private System.Windows.Forms.RibbonPanel ribbonPanel22;
        private System.Windows.Forms.RibbonButton btnTopluUye;
        private System.Windows.Forms.RibbonPanel ribbonPanel23;
        private System.Windows.Forms.RibbonButton btnExcelKitapEkle;
        private System.Windows.Forms.RibbonPanel ribbonPanel24;
        private System.Windows.Forms.RibbonButton btnStatistikDuzenle;
        private System.Windows.Forms.RibbonPanel ribbonPanel25;
        private System.Windows.Forms.RibbonButton btnAppGuncelle;
    }
}