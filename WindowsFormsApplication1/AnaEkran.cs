﻿using ktProje.Kontroller;
using ktProje.Kontroller.Ayarlar;
using ktProje.Kontroller.Istatistik;
using ktProje.Kontroller.Kitaplar;
using ktProje.Kontroller.Odunc;
using ktProje.Kontroller.Uyeler;
using System;
using System.Windows.Forms;

namespace ktProje
{
    public partial class AnaEkran : Form
    {
        public AnaEkran()
        {
            InitializeComponent();
        }

        public void PaneliTemizle()
        {
            if (anaPanel.Controls.Count > 0)
            {
                anaPanel.Controls[0].Dispose();
            }
            anaPanel.Controls.Clear();
            anaPanel.BackgroundImage = null;
        }

        private void PaneliYenile()
        {
            if (anaPanel.Controls.Count > 0)
            {
                anaPanel.Controls[0].Dispose();
            }
            anaPanel.Controls.Clear();
            anaPanel.BackgroundImage = ktProje.Properties.Resources.OAYXJ60;
        }

        private void PaneliOrtala()
        {
            if (anaPanel.Controls.Count > 0)
            {
                anaPanel.Controls[0].Left = (anaPanel.Width - anaPanel.Controls[0].Width) / 2;
                anaPanel.Controls[0].Top = (anaPanel.Height - anaPanel.Controls[0].Height) / 2;
            }
        }

        private void AnaEkran_SizeChanged(object sender, EventArgs e)
        {
            PaneliOrtala();
        }

        private void AnaEkran_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + "  " + Temel_Metodlar.VersiyonNumarasıGetir();
        }


        #region Menü Butonları İşlemleri

        private void ribbon1_ActiveTabChanged(object sender, EventArgs e)
        {
            PaneliYenile();
        }



        private void ribbonOrbMenuItem1_Click(object sender, EventArgs e)
        {
            PaneliYenile();
            ribbon1.ActiveTab = ribbonTab1;
        }

        private void ribbonOrbMenuItem2_Click(object sender, EventArgs e)
        {
            PaneliYenile();
            ribbon1.ActiveTab = ribbonTab1;
        }

        private void ribbonOrbMenuItem3_Click(object sender, EventArgs e)
        {
            PaneliYenile();
            ribbon1.ActiveTab = ribbonTab2;
        }

        private void ribbonOrbMenuItem4_Click(object sender, EventArgs e)
        {
            PaneliYenile();
            ribbon1.ActiveTab = ribbonTab4;
        }

        private void ribbonOrbMenuItem5_Click(object sender, EventArgs e)
        {
            PaneliYenile();
            ribbon1.ActiveTab = ribbonTab3;
        }

        private void ribbonOrbMenuItem6_Click(object sender, EventArgs e)
        {
            PaneliYenile();
            ribbon1.ActiveTab = ribbonTab5;
        }

        private void ribbonOrbMenuItem7_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }



        #endregion

        #region Kitap İşlemleri

        private void btnKitapListesi_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCkitapListesi kitapListe = new UCkitapListesi();
            anaPanel.Controls.Add(kitapListe);
            PaneliOrtala();
        }

        private void btnKitapEkle_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCkitapEkle kitapEkle = new UCkitapEkle();
            anaPanel.Controls.Add(kitapEkle);
            PaneliOrtala();
        }

        private void btnKitapDüzenle_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCkitapDuzenle kitapDuzenle = new UCkitapDuzenle();
            anaPanel.Controls.Add(kitapDuzenle);
            PaneliOrtala();
        }

        private void btnKitapSil_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCkitapSil kitapSil = new UCkitapSil();
            anaPanel.Controls.Add(kitapSil);
            PaneliOrtala();
        }

        private void ribbonButton1_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCtopluKitapDuzenle duzenle = new UCtopluKitapDuzenle();
            anaPanel.Controls.Add(duzenle);
            PaneliOrtala();
        }


        #endregion

        #region Üye İşlemleri

        private void btnUyeListesi_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCuyeListele uyeListele = new UCuyeListele();
            anaPanel.Controls.Add(uyeListele);
            PaneliOrtala();
        }

        private void btnUyeEkle_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCuyeEkle uyeEkle = new UCuyeEkle();
            anaPanel.Controls.Add(uyeEkle);
            PaneliOrtala();
        }

        private void btnUyeDuzenle_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCuyeDuzenle uyeDuzenle = new UCuyeDuzenle();
            anaPanel.Controls.Add(uyeDuzenle);
            PaneliOrtala();
        }

        private void btnUyeSil_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCuyeSil uyeDuzenle = new UCuyeSil();
            anaPanel.Controls.Add(uyeDuzenle);
            PaneliOrtala();
        }

        private void btnUyeTopluDuzenle_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCuyeTopluDuzenle uyeTopluDuzenle = new UCuyeTopluDuzenle();
            anaPanel.Controls.Add(uyeTopluDuzenle);
            PaneliOrtala();
        }


        #endregion

        #region Ödünç İşlemleri

        private void btnOduncVer_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCoduncVer oduncVer = new UCoduncVer();
            anaPanel.Controls.Add(oduncVer);
            PaneliOrtala();
        }

        private void btnUyedekiKitaplar_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCuyedekiKitaplar uyedekiler = new UCuyedekiKitaplar();
            anaPanel.Controls.Add(uyedekiler);
            PaneliOrtala();
        }

        private void btnTeslimAl_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCoduncTeslimAl teslim = new UCoduncTeslimAl();
            anaPanel.Controls.Add(teslim);
            PaneliOrtala();
        }

        private void btnSuresiGecen_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCsuresiGecenler sureGecenler = new UCsuresiGecenler();
            anaPanel.Controls.Add(sureGecenler);
            PaneliOrtala();
        }


        #endregion

        #region Ayar İşlemleri

        private void btnGenelAyarlar_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCayarlar ayar = new UCayarlar();
            anaPanel.Controls.Add(ayar);
            PaneliOrtala();
        }

        private void btnBarcode_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCbarkod barkod = new UCbarkod();
            anaPanel.Controls.Add(barkod);
            PaneliOrtala();
        }

        private void ribbonButton2_Click_1(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCsinifEkle sinif = new UCsinifEkle();
            anaPanel.Controls.Add(sinif);
            PaneliOrtala();
        }

        private void btnYayineviDuz_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCyayineviEkle yayinEvi = new UCyayineviEkle();
            anaPanel.Controls.Add(yayinEvi);
            PaneliOrtala();
        }

        private void btnAppGuncelle_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCguncelle guncelleme = new UCguncelle();
            anaPanel.Controls.Add(guncelleme);
            PaneliOrtala();
        }

        #endregion

        #region İstatistik İŞlemleri

        private void btnKitapOkuyanlar_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCkitapOkuyanlar kitapOkuyanlar = new UCkitapOkuyanlar();
            anaPanel.Controls.Add(kitapOkuyanlar);
            PaneliOrtala();
        }

        private void btnOkunanKitaplar_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCokunanKitaplar okunanKitaplar = new UCokunanKitaplar();
            anaPanel.Controls.Add(okunanKitaplar);
            PaneliOrtala();

        }

        private void btnOgrOkuduguKitaplar_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCogrenciOkunanlar ogrOkunanKitaplar = new UCogrenciOkunanlar();
            anaPanel.Controls.Add(ogrOkunanKitaplar);
            PaneliOrtala();
        }

        private void btnStatistikDuzenle_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCstatistikDuzenle istatistikDuzenle = new UCstatistikDuzenle();
            anaPanel.Controls.Add(istatistikDuzenle);
            PaneliOrtala();
        }

        #endregion

        private void btnTopluUye_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCuyeTopluEkle duzenle = new UCuyeTopluEkle();
            anaPanel.Controls.Add(duzenle);
            PaneliOrtala();
        }

        private void btnExcelKitapEkle_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCtopluKitapEkle topluKitap = new UCtopluKitapEkle();
            anaPanel.Controls.Add(topluKitap);
            PaneliOrtala();
        }

       
    }
}
