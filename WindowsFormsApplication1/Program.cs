﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace ktProje
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (System.Diagnostics.Process.GetProcessesByName("ktProje").Length > 1)
            {
                MessageBox.Show("Program zaten açık.", "Kütüphane Kayıt İşlemleri", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
            else
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new AnaEkran());
            }
        }
    }
}
